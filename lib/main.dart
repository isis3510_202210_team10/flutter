import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/pages/sign_in.dart';
import 'package:in_shop_flutter/src/services/locator.dart';

/*
 * Service locator for handling the singleton pattern
 */
final getIt = GetIt.instance;

void main() {
  /*
   * Define service locator for singleton and shared preferences
   */
  setup();

  /*
   * Application display configuration
   */
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) => runApp(const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'InShop',
      debugShowCheckedModeBanner: false,
      initialRoute: SignInPage.id,
      routes: {
        SignInPage.id: (context) => const SignInPage(),
      },
    );
  }
}
