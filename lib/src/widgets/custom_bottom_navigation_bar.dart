import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/pages/dasboard.dart';
import '../pages/development_page.dart';
import '../pages/home.dart';
import '../pages/orders_shops.dart';
import '../pages/orders_suppliers.dart';
import '../services/shared_prefs_service.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  const CustomBottomNavigationBar({Key? key}) : super(key: key);

  @override
  State<CustomBottomNavigationBar> createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int index = 0;

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  @override
  void initState() {
    super.initState();
    userData();
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      showSelectedLabels: false,
      currentIndex: index,
      onTap: (int index) {
        setState(() {
          this.index = index;
        });
        if (index == 0) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => HomePage()));
        }
        if (index == 1) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => (type == "suppliers")
                  ? const OrdersSuppliersPage()
                  : const OrderShopsPage()));
        }
        if (index == 2) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => DashboardPage()));
        }
        if (index == 3) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => DevelopmentPage()));
        }
      },
      showUnselectedLabels: false,
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/store.png"),
              color: Color(0xFF000000),
              size: 35,
            ),
            label: "Home"),
        BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/list.png"),
              color: Color(0xFF000000),
              size: 43,
            ),
            label: "Inventory"),
        BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/bar-graph.png"),
              color: Color(0xFF000000),
              size: 35,
            ),
            label: "Dashboard"),
        BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("images/gear.png"),
              color: Color(0xFF000000),
              size: 35,
            ),
            label: "Settings"),
      ],
      backgroundColor: const Color(0xFFD5A021),
    );
  }
}
