import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../services/shared_prefs_service.dart';

class CustomBuilder extends StatefulWidget {
  const CustomBuilder({Key? key}) : super(key: key);

  @override
  State<CustomBuilder> createState() => _CustomBuilderState();
}

class _CustomBuilderState extends State<CustomBuilder> {
  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();
  File? _image;

  loadPhoto() async {
    try {
      String? profilePath =
          await _sharedPreferencesService.getStringValue("profile_image");

      if (profilePath != "") {
        setState(() {
          _image = File(profilePath);
        });
      }
    } catch (e) {
      //pass
    }
  }

  @override
  void initState() {
    super.initState();
    loadPhoto();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) => Container(
        padding: const EdgeInsets.only(top: 5, left: 5, bottom: 5, right: 9),
        child: InkWell(
          onTap: () =>
              Scaffold.of(context).openEndDrawer(), // Handle your callback.
          splashColor: Colors.brown.withOpacity(0.5),
          child: CircleAvatar(
            radius: MediaQuery.of(context).size.width / 16, // Image radius
            backgroundImage: _image == null
                ? const AssetImage("images/user.png") as ImageProvider
                : FileImage(_image!),
          ),
        ),
      ),
    );
  }
}
