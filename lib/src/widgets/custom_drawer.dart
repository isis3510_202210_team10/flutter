import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:path_provider/path_provider.dart';

import '../pages/profile.dart';
import '../pages/sign_in.dart';

import '../services/shared_prefs_service.dart';
import 'package:in_shop_flutter/src/database/orders_database.dart';
import 'package:in_shop_flutter/src/database/shop_product_database.dart';
import 'package:in_shop_flutter/src/database/suppiler_product_database.dart';
import '../services/sqlite_service.dart';

class CustomDrawer extends StatefulWidget {
  var inventory;
  CustomDrawer({Key? key, this.inventory}) : super(key: key);

  @override
  State<CustomDrawer> createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();
  static final items = ['Profile', 'Log out'];
  File? _image;

  String name = "Company";
  String type = "food";

  updateData() async {
    var n = await _sharedPreferencesService.getStringValue("name");
    var t = await _sharedPreferencesService.getStringValue("type");
    setState(() {
      name = n;
      type = t;
    });
  }

  loadPhoto() async {
    try {
      String? profilePath =
          await _sharedPreferencesService.getStringValue("profile_image");

      if (profilePath != "") {
        setState(() {
          _image = File(profilePath);
        });
      }
    } catch (e) {
      //pass
    }
  }

  late OrdersDatabase orders;
  late ShopProductDatabase shopP;
  late SupplierProductDatabase supplierP;

  final SqliteService _sqliteService = GetIt.I.get<SqliteService>();

  deleteDataBases() async {
    await _sharedPreferencesService.removeAll();
    String fileName = "UserInfo.json";
    var dir = await getTemporaryDirectory();
    File file = File(dir.path + "/" + fileName);
    if(file.existsSync())
    {
      file.deleteSync();
    }
    $FloorOrdersDatabase
        .databaseBuilder('orders_database.db')
        .build()
        .then((value) async {
      orders = value;
      await _sqliteService.deleteOrders(orders); // clean local db
    });
    $FloorShopProductDatabase
        .databaseBuilder('shop_product_database.db')
        .build()
        .then((value) async {
      shopP = value;
      await _sqliteService.deleteShopProducts(shopP); // clean local db
    });
    $FloorSupplierProductDatabase
        .databaseBuilder('suppiler_product_database.db')
        .build()
        .then((value) async {
      supplierP = value;
      await _sqliteService.deleteSupplierProducts(supplierP); // clean local db
    });
  }

  @override
  void initState() {
    super.initState();
    updateData();
    loadPhoto();
    
  }



  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 250,
      child: Drawer(
        backgroundColor: const Color(0xFF243B4A),
        child: ListView(
          padding: const EdgeInsets.only(top: 40),
          children: <Widget>[
            DrawerHeader(
              decoration: const BoxDecoration(color: Color(0xFF243B4A)),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: CircleAvatar(
                      backgroundImage: _image == null
                          ? const AssetImage("images/user.png") as ImageProvider
                          : FileImage(_image!),
                      radius: 50,
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter + const Alignment(0, 0.5),
                    child: Text(
                      name.toString(),
                      style: const TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter + const Alignment(0, 0.8),
                    child: Text(
                      type.toString(),
                      style: const TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: widget.inventory
                  ? const EdgeInsets.only(
                      top: 40,
                    )
                  : const EdgeInsets.only(top: 40, bottom: 20),
              transform: Matrix4.translationValues(0.0, 20.0, 0.0),
              decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.white))),
            ),
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: items.length,
              itemBuilder: (context, index) {
                return Container(
                  decoration: const BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.white))),
                  child: ListTile(
                    title: Text(
                      items[index],
                      style: const TextStyle(fontSize: 20),
                    ),
                    onTap: () async {
                      if (index == 0) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const ProfilePage()));
                      } else if (index == 1) {
                        await deleteDataBases();
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const SignInPage()));
                      }
                    },
                    textColor: const Color(0xFFFFFFFF),
                  ),
                );
              },
            ),
          ],
        ), // Populate the Drawer in the next step.
      ),
    );
  }
}
