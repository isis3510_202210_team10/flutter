import 'dart:async';
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/database/supplier_database.dart';
import 'package:in_shop_flutter/src/models/shop_product.dart';
import 'package:in_shop_flutter/src/models/supplier.dart';
import 'package:in_shop_flutter/src/pages/dasboard.dart';
import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';
import 'package:in_shop_flutter/src/pages/products.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../services/connectivity_service.dart';
import '../services/shared_prefs_service.dart';
import '../services/sqlite_service.dart';
import 'orders_suppliers.dart';

class SuppliersPage extends StatefulWidget {
  static String id = 'orders';

  const SuppliersPage({Key? key}) : super(key: key);

  @override
  State<SuppliersPage> createState() => _SuppliersPageState();
}

class _SuppliersPageState extends State<SuppliersPage> {
  List<Supplier> suppliers = [];
  final items = ['Profile', 'Log out'];
  final filter = [
    'CompanyName1',
    'CompanyName2',
    'CompanyName3',
    'CompanyName4',
    'CompanyName5',
    'CompanyName6',
    'CompanyName7',
    'CompanyName8',
    'CompanyName9',
    'CompanyName10',
    'CompanyName11'
  ];
  String? selectedItem;
  GlobalKey navBarGlobalKey = GlobalKey(debugLabel: 'bottomAppBar');
  int index = 0;
  late String destination;

  fetchSuppliers() async {
    String baseUrl = "https://inshop-uniandes.herokuapp.com/suppliers";
    try {
      var response = await http.get(Uri.parse(baseUrl));
      if (response.statusCode == 200) {
        var items = json.decode(response.body);
        List<Supplier> supps =
            List<Supplier>.from(items.map((supp) => Supplier.fromJson(supp)));
        return supps;
      } else {
        return Future.error("Server Error");
      }
    } catch (e) {
      return Future.error(e);
    }
  }

  late SupplierDatabase database;

  // conectivity
  bool isConected = true;
  bool firstTime = true;

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  final ConnectivityService _connectivityService =
      GetIt.I.get<ConnectivityService>();

  final SqliteService _sqliteService = GetIt.I.get<SqliteService>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    if (mounted) {
      setState(() {
        type = typeUser;
      });
    }
  }

  Stream<bool> getConnection() async* {
    while (true) {
      await Future.delayed(const Duration(milliseconds: 500));
      yield await _connectivityService.checkInternetConnection();
    }
  }

  @override
  void initState() {
    super.initState();
    userData();
    getConnection().listen((value) {
      if (mounted) {
        setState(() {
          if (!isConected && value || firstTime && value) {
            fetchState();
            isConected = true;
            firstTime = false;
            print("XDDDDD");
          } else if (!value) {
            isConected = false;
          } else {
            isConected = true;
          }
        });
      }
    });
    $FloorSupplierDatabase
        .databaseBuilder('supplier_database.db')
        .build()
        .then((value) async {
      database = value;
      List<Supplier> suppliersDB =
          await _sqliteService.retrieveSuppliers(database);
      if (mounted) {
        setState(() {
          suppliers = suppliersDB;
          print(suppliers.length);
          print("holaaa");
        });
      }
    });
  }

  fetchState() async {
    List<Supplier> supp = await fetchSuppliers();
    if (mounted) {
      setState(() {
        suppliers = supp;
      });
      await _sqliteService.deleteSuppliers(database); // clean local db
      await _sqliteService.addSuppliers(
          database, suppliers); // save in local db
    }
  }

  /*
   * Dispose for connectivity management
   */
  @override
  dispose() {
    super.dispose();
  }

  Future<void> save_sharedPrefereces(idSupplier) async {
    final prefs = await SharedPreferences.getInstance();

    // Save an String value to 'idSupplier' key.
    await prefs.setString('idSupplier', idSupplier);
  }

  String urlSupplier() {
    String url = "https://picsum.photos/50/50";
    return url;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Suppliers",
            style: TextStyle(
              fontSize: MediaQuery.of(context).size.height * 0.03,
            ),
          ),
          leading: BackButton(
              color: Colors.white,
              onPressed: () {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => HomePage()));
              }),
          actions: const [
            CustomBuilder(),
          ],
          backgroundColor: const Color(0xFFD5A021),
        ),
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          currentIndex: index,
          onTap: (int index) {
            setState(() {
              this.index = index;
            });
            if (index == 0) {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => HomePage()));
            }
            if (index == 1) {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => (type == "suppliers")
                      ? const OrdersSuppliersPage()
                      : const OrderShopsPage()));
            }
            if (index == 2) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => DashboardPage()));
            }
            if (index == 3) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => DevelopmentPage()));
            }
          },
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/store.png"),
                  color: Color(0xFF000000),
                  size: 35,
                ),
                label: "Home"),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/list.png"),
                  color: Color(0xFF000000),
                  size: 43,
                ),
                label: "Inventory"),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/bar-graph.png"),
                  color: Color(0xFF000000),
                  size: 35,
                ),
                label: "Dashboard"),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/gear.png"),
                  color: Color(0xFF000000),
                  size: 35,
                ),
                label: "Settings"),
          ],
          backgroundColor: const Color(0xFFD5A021),
        ),
        endDrawer: CustomDrawer(inventory: true),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              !isConected
                  ? IntrinsicWidth(
                      child: Container(
                        height: 25,
                        width: MediaQuery.of(context).size.width,
                        color: const Color(0xFF7A93AC),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: const [
                            Text("No internet connection"),
                          ],
                        ),
                      ),
                    )
                  : Container(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.all(25.0),
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.80,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: const Color(0xFF7A93AC),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text(
                          "Suppliers",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              "Filter:",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(width: 15),
                            Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.026,
                                width: MediaQuery.of(context).size.width * 0.60,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFCFDBD5),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(15.0),
                                  ),
                                ),
                                child: DropdownButton(
                                    isExpanded: true,
                                    items: filter.map((String s) {
                                      return DropdownMenuItem(
                                          value: s, child: Text(s));
                                    }).toList(),
                                    onChanged: (String? value) => setState(() {
                                          selectedItem = value ?? "";
                                        }),
                                    hint: Center(
                                        child: Text(
                                            selectedItem ?? "Select Filter"))))
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 23),
                      height: MediaQuery.of(context).size.height * 0.5,
                      width: MediaQuery.of(context).size.width * 0.87,
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ListView.builder(
                                addAutomaticKeepAlives: false,
                                addRepaintBoundaries: false, 
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: suppliers.length,
                                itemBuilder: (context, i) {
                                  if (selectedItem == suppliers[i].name ||
                                      selectedItem == null) {
                                    int lengthAdress =
                                        suppliers[i].address.length;
                                    if (lengthAdress > 20) {
                                      lengthAdress = 20;
                                    }
                                    return Container(
                                      padding: const EdgeInsets.only(
                                          top: 15, left: 15, right: 15),
                                      margin: const EdgeInsets.all(7.0),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.19,
                                      width: MediaQuery.of(context).size.width *
                                          0.83,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                        color: const Color(0xFFD5A021),
                                      ),
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              const Text(
                                                "Supplier ID: ",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Text(suppliers[i]
                                                  .id
                                                  .toString()
                                                  .substring(0, 14)),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.16,
                                              ),
                                            ],
                                          ),
                                          Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.0066,
                                          ),
                                          Row(
                                            children: [
                                              CachedNetworkImage(
                                                imageUrl: urlSupplier(),
                                                placeholder: (context, url) =>
                                                    const ColoredBox(
                                                  color: Color(0xFFD5A021),
                                                  child: Center(
                                                      child:
                                                          CircularProgressIndicator()),
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        const ColoredBox(
                                                  color: Color(0xFFD5A021),
                                                  child: Icon(Icons.error,
                                                      size: 50,
                                                      color: Colors.black),
                                                ),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.01,
                                              ),
                                              Column(
                                                mainAxisSize: MainAxisSize.min,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Wrap(children: [
                                                    const Text(
                                                      "Name: ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(suppliers[i]
                                                        .companyName)
                                                  ]),
                                                  Row(children: [
                                                    const Text(
                                                      "Phone: ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(suppliers[i].phone)
                                                  ]),
                                                  Wrap(
                                                      direction:
                                                          Axis.horizontal,
                                                      children: <Widget>[
                                                        const Text(
                                                          "Address: ",
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        Text(suppliers[i]
                                                            .address
                                                            .substring(0,
                                                                lengthAdress))
                                                      ]),
                                                ],
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                transform:
                                                    Matrix4.translationValues(
                                                        0.0, -1.0, 0.0),
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.04,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.15,
                                              ),
                                              Expanded(
                                                  flex: 5,
                                                  child: Container(
                                                    color:
                                                        const Color(0xFFD5A021),
                                                  )),
                                              const SizedBox(
                                                height: 40,
                                              ),
                                              Container(
                                                margin: const EdgeInsets.only(
                                                    left: 2),
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.05,
                                                width: 90,
                                                child: TextButton(
                                                  child: const Text(
                                                    "Catalog",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 13),
                                                  ),
                                                  onPressed: () {
                                                    save_sharedPrefereces(
                                                        suppliers[i].id);
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                const ProductsPage()));
                                                  },
                                                ),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                  color:
                                                      const Color(0xFF243B4A),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  } else {
                                    return const Center(
                                      child: Text("Nothing found in search"),
                                    );
                                  }
                                }),
                          ],
                        ),
                      ),
                    ),
                  ]),
            ],
          ),
        ));
  }
}
