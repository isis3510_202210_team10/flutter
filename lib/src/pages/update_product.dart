import 'dart:async';
import 'dart:isolate';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/inventory_shop.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';
import 'package:in_shop_flutter/src/models/shop_product.dart';
import 'package:in_shop_flutter/src/database/shop_product_database.dart';
import 'package:in_shop_flutter/src/pages/products.dart';
import 'package:in_shop_flutter/src/services/isolate_service.dart';
import 'package:in_shop_flutter/src/services/sqlite_service.dart';
import '../services/connectivity_service.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import "package:flutter/src/services/text_formatter.dart";

import '../services/shared_prefs_service.dart';
import 'orders_suppliers.dart';

class UpdateProductPage extends StatefulWidget {
  static String id = 'UpdateProductPage';
  var products;
  UpdateProductPage({Key? key, this.products}) : super(key: key);

  @override
  State<UpdateProductPage> createState() => _UpdateProductPageState();
}

class _UpdateProductPageState extends State<UpdateProductPage> {
  List<ShopProduct> products = [];
  final formKey = GlobalKey<FormState>();

  /*
   * Registration form text field controllers
  */
  TextEditingController _nameController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();

  TextEditingController _stockController = TextEditingController();

  late ShopProductDatabase database;

  // ID
  String productID = "Choose a Product ID";

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  void savePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("nameP", _nameController.text);
    prefs.setString("descriptionP", _descriptionController.text);
    prefs.setString("stockP", _stockController.text);
    prefs.setString("pID", productID);
    print("saved the sumbit fields!");
  }

  void loadPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var nameP = prefs.get("nameP").toString();
    if(nameP != "null")
    {
      _nameController = TextEditingController(text: nameP);
    }
    var descriptionP = prefs.get("descriptionP").toString();
    if(descriptionP != "null")
    {
      _descriptionController = TextEditingController(text: descriptionP);
    }
    var stockP =  prefs.get("stockP").toString();
    if(stockP != "null")
    {
      _stockController= TextEditingController(text: stockP);
    }
    var pID = prefs.get("pID").toString();
    if(pID != "null")
    {
      productID = pID;
    }
  }

  final ConnectivityService _connectivityService =
      GetIt.I.get<ConnectivityService>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  @override
  void initState() {
    super.initState();
    print(widget.products[0].name);
    userData();
    loadPrefs();
  }

  /*
   * Dispose for connectivity management
   */
  @override
  dispose() {
    super.dispose();
  }

  final IsolateService _isolateService = GetIt.I.get<IsolateService>();

  bool isLoading = false;

  Future<bool> _checkInternet() async {
    return _connectivityService.checkInternetConnection();
  }

  ShopProduct findShopProduct(List<ShopProduct> prods, String id) {
    ShopProduct rta = prods[0];
    for (var i = 0; i < prods.length; i++) {
      ShopProduct act = prods[i];
      if (act.id == id) {
        rta = act;
        break;
      }
    }
    return rta;
  }

  updateProduct() async {
    final receivePort = ReceivePort();
    String id = await _sharedPreferencesService.getStringValue("id");
    ShopProduct sp = ShopProduct(
        id: "IDShopProduct32",
        name: "PruebaUpdate",
        description: "description",
        type: "FOOD",
        image:
            "https://i.pinimg.com/474x/3b/44/b8/3b44b80324a0b2ed36012647684d6d32.jpg",
        expirationDate: "2022-05-29",
        priceSupplier: "20",
        stock: "1",
        shopId: id);
    await Isolate.spawn(
      _isolateService.updateProduct,
      [receivePort.sendPort, sp],
    );
    print("almost finish");
    var res = await receivePort.first;
    return res;
  }

  final ScrollController _mycontroller = ScrollController();
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: const Color(0xFF7A93AC),
      appBar: AppBar(
        title: Text(
          "Back",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: const [
          CustomBuilder(),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      endDrawer: CustomDrawer(inventory: false),
      body: SingleChildScrollView(
        child: Form(
          onChanged: savePrefs,
          key: formKey,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 23.0),
              child: Column(
                children: [
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildBanner(),
                  const SizedBox(
                    height: 15.0,
                  ),
                  buildID(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildName(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildDescription(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildStock(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildFunctionButtom(),
                  //const SizedBox(
                  //height: 15.0,
                  //),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        currentIndex: index,
        onTap: (int index) {
          setState(() {
            this.index = index;
          });
          if (index == 0) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => HomePage()));
          }
          if (index == 1) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => (type == "suppliers")
                    ? const OrdersSuppliersPage()
                    : const OrderShopsPage()));
          }
          if (index == 2) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
          if (index == 3) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
        },
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/store.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/list.png"),
                color: Color(0xFF000000),
                size: 43,
              ),
              label: "Inventory"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/bar-graph.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Dashboard"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/gear.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Settings"),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
    ));
  }

  /*
   * Widget containing the elements that make up the banner
   */
  Widget buildBanner() => const Text(
        'Update Product',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
      );

  Widget buildID() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Product ID',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: DropdownButtonFormField(
              hint: Text(
                productID,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 13,
                ),
              ),
              isExpanded: true,
              validator: (value) {
                if (value == null) {
                  return "field required";
                } else {
                  return null;
                }
              },
              iconSize: 30.0,
              style: const TextStyle(color: Colors.black),
              items: widget.products
                      ?.map<DropdownMenuItem<String>>(
                          (value) => DropdownMenuItem<String>(
                                value: value.id,
                                child: Text(value.id),
                              ))
                      ?.toList() ??
                  [],
              onChanged: (val) {
                setState(() {
                  productID = val.toString();
                  print(val.toString());
                });
              },
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field company name
   */
  Widget buildName() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Product Name',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
              maxLength: 20,
              controller: _nameController,
              decoration: InputDecoration(
                  labelText: 'Product Name',
                  filled: true,
                  fillColor: const Color(0xFFCFDBD5),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none),
                  counterText: '',
                  errorStyle:
                      const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  isDense: true),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter valid product name';
                } else if (value.length < 3) {
                  return 'Enter at least 3 characters';
                } else if (value.length > 20) {
                  return 'Enter maximum 20 characters';
                } else {
                  return null;
                }
              },
            ),
          )
        ],
      );

  Widget buildDescription() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Product Description',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
              maxLength: 20,
              controller: _descriptionController,
              decoration: InputDecoration(
                  labelText: 'Product Description',
                  filled: true,
                  fillColor: const Color(0xFFCFDBD5),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none),
                  counterText: '',
                  errorStyle:
                      const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  isDense: true),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter valid product description';
                } else if (value.length < 4) {
                  return 'Enter at least 4 characters';
                } else if (value.length > 50) {
                  return 'Enter maximum 50 characters';
                } else {
                  return null;
                }
              },
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field company phone
   */
  Widget buildStock() => Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: const EdgeInsets.only(left: 55.0),
            child: const Text(
              'Stock',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
            ),
          ),
        ),
        const SizedBox(
          height: 5.0,
        ),
        SizedBox(
          width: 250.0,
          child: TextFormField(
              maxLength: 10,
              controller: _stockController,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp("[0-9.]")),
              ],
              decoration: InputDecoration(
                  filled: true,
                  fillColor: const Color(0xFFCFDBD5),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none),
                  labelText: 'Stock',
                  counterText: '',
                  errorStyle:
                      const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  isDense: true),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter a valid stock';
                } else if (int.parse(value.trim()) < 0) {
                  return 'Stock cannot be negative';
                } else if (!RegExp(
                        r"^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$")
                    .hasMatch(value)) {
                  return 'Phone is not formatted correctly';
                } else {
                  return null;
                }
              }),
        )
      ]);

  Widget buildFunctionButtom() => ElevatedButton(
        onPressed: () async {
          const url = 'https://inshop-uniandes.herokuapp.com/shopProducts';
          final con = await _checkInternet();
          if (con.toString() == "true") {
            if (formKey.currentState!.validate()) {
              setState(() => isLoading = true);
              ShopProduct act = findShopProduct(widget.products, productID);
              ShopProduct prod = ShopProduct(
                  id: productID,
                  name: _nameController.text,
                  description: _descriptionController.text,
                  type: act.type,
                  image: act.image,
                  expirationDate: act.expirationDate,
                  priceSupplier: act.priceSupplier,
                  stock: _stockController.text,
                  shopId: act.shopId);
              final receivePort = ReceivePort();
              await Isolate.spawn(
                _isolateService.updateProduct,
                [receivePort.sendPort, prod],
              );
              var res = await receivePort.first;
              setState(() => isLoading = false);
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const InventoryShopPage()));
            }
          } else {
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      title: const Text('Network connection problem'),
                      content: const Text(
                          'The update functionality is not available since it is not connected to the WIFI/Mobile network.'),
                      backgroundColor: Colors.grey,
                      actions: [
                        TextButton(
                            child: const Text('Ok',
                                style: TextStyle(color: Colors.black)),
                            onPressed: () => Navigator.of(context).pop())
                      ],
                    ));
          }
        },
        child: isLoading
            ? SizedBox(
                width: 150,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SizedBox(
                      child: CircularProgressIndicator(
                          color: Colors.black, strokeWidth: 2),
                      height: 15.0,
                      width: 15.0,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Please Wait...',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: Colors.black),
                    )
                  ],
                ))
            : const Text(
                'Confirm',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: Colors.black),
              ),
        style: ElevatedButton.styleFrom(
          primary: const Color(0xFFCFDBD5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
      );
}
