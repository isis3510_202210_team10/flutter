import 'dart:async';
import 'dart:isolate';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';
import 'package:in_shop_flutter/src/models/orders.dart';
import 'package:in_shop_flutter/src/database/orders_database.dart';
import 'package:in_shop_flutter/src/services/isolate_service.dart';
import 'package:in_shop_flutter/src/services/sqlite_service.dart';
import '../services/connectivity_service.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import "package:flutter/src/services/text_formatter.dart";
import 'package:intl/intl.dart';
import '../pages/orders_suppliers.dart';

import '../services/shared_prefs_service.dart';
import 'orders_suppliers.dart';

class UpdateOrderPage extends StatefulWidget {
  static String id = 'UpdateOrderPage';
  var order;
  UpdateOrderPage({Key? key, this.order}) : super(key: key);

  @override
  State<UpdateOrderPage> createState() => _UpdateOrderPageState();
}

class _UpdateOrderPageState extends State<UpdateOrderPage> {
  List<Orders> orders = [];
  final formKey = GlobalKey<FormState>();
  final status = [
    'ON_REVISION',
    'ACCEPTED',
    'DECLINED',
    'SHIPPING',
    'COMPLETED'
  ];

  String orderStatus = "Choose an order status";

  /*
   * Registration form text field controllers
  */
  TextEditingController _dateController = TextEditingController();
  TextEditingController _addressController = TextEditingController();

  TextEditingController _totalController = TextEditingController();

  late OrdersDatabase database;

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  void savePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("date", _dateController.text);
    prefs.setString("address", _addressController.text);
    prefs.setString("total", _totalController.text);
    prefs.setString("status", orderStatus);
    print("saved the sumbit fields!");
  }

  void loadPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var date = prefs.get("date").toString();
    if(date != "null")
    {
      _dateController = TextEditingController(text: date);
    }
    var address = prefs.get("address").toString();
    if(address != "null")
    {
      _addressController = TextEditingController(text: address);
    }
    var total =  prefs.get("total").toString();
    if(total != "null")
    {
      _totalController= TextEditingController(text: total);
    }
    var status = prefs.get("status").toString();
    if(status != "null")
    {
      orderStatus = status;
    }

  }

  final ConnectivityService _connectivityService =
      GetIt.I.get<ConnectivityService>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  @override
  void initState() {
    super.initState();
    print(widget.order);
    userData();
    loadPrefs();
  }

  /*
   * Dispose for connectivity management
   */
  @override
  dispose() {
    super.dispose();
  }

  final IsolateService _isolateService = GetIt.I.get<IsolateService>();

  bool isLoading = false;

  Future<bool> _checkInternet() async {
    return _connectivityService.checkInternetConnection();
  }

  int index = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: const Color(0xFF7A93AC),
      appBar: AppBar(
        title: Text(
          "Back",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: const [
          CustomBuilder(),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      endDrawer: CustomDrawer(inventory: false),
      body: SingleChildScrollView(
        child: Form(
          onChanged: savePrefs,
          key: formKey,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 23.0),
              child: Column(
                children: [
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildBanner(),
                  const SizedBox(
                    height: 15.0,
                  ),
                  buildID(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildDate(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildAddress(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildTotal(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildStatus(),
                  const SizedBox(
                    height: 5.0,
                  ),
                  buildFunctionButtom(),
                  //const SizedBox(
                  //height: 15.0,
                  //),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        currentIndex: index,
        onTap: (int index) {
          setState(() {
            this.index = index;
          });
          if (index == 0) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => HomePage()));
          }
          if (index == 1) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => (type == "suppliers")
                    ? const OrdersSuppliersPage()
                    : const OrderShopsPage()));
          }
          if (index == 2) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
          if (index == 3) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
        },
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/store.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/list.png"),
                color: Color(0xFF000000),
                size: 43,
              ),
              label: "Inventory"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/bar-graph.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Dashboard"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/gear.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Settings"),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
    ));
  }

  /*
   * Widget containing the elements that make up the banner
   */
  Widget buildBanner() => const Text(
        'Update Purchase Order',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
      );

  Widget buildID() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: Text(
                'Order ID: '+widget.order.id.toString(),
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              )
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
        ],
      );

  Widget buildStatus() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Order Status',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: DropdownButtonFormField(
              hint: Text(
                orderStatus,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 13,
                ),
              ),
              isExpanded: true,
              validator: (value) {
                if (value == null) {
                  return "field required";
                } else {
                  return null;
                }
              },
              iconSize: 30.0,
              style: const TextStyle(color: Colors.black),
              items: status
                      .map<DropdownMenuItem<String>>(
                          (value) => DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              ))
                      .toList(),
              onChanged: (val) {
                setState(() {
                  orderStatus = val.toString();
                  print(val.toString());
                });
              },
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field company name
   */
  Widget buildDate() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Delivery date',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
              controller: _dateController, //editing controller of this TextField
              decoration: const InputDecoration( 
                   icon: Icon(Icons.calendar_today), //icon of text field
                   labelText: "Enter Date" //label text of field
              ),
              readOnly: true,  //set it true, so that user will not able to edit text
              onTap: () async {
                DateTime? pickedDate = await showDatePicker(
                      context: context, initialDate: DateTime.now(),
                      firstDate: DateTime(2000), //DateTime.now() - not to allow to choose before today.
                      lastDate: DateTime(2101)
                );
                  
                  if(pickedDate != null ){
                      print(pickedDate);  //pickedDate output format => 2021-03-10 00:00:00.000
                      String formattedDate = DateFormat('yyyy-MM-dd').format(pickedDate); 
                      print(formattedDate); //formatted date output using intl package =>  2021-03-16
                        //you can implement different kind of Date Format here according to your requirement

                      setState(() {
                         _dateController.text = formattedDate; //set output date to TextField value. 
                      });
                  }else{
                      print("Date is not selected");
                  }
                },
            ),
          )
        ],
      );

  Widget buildAddress() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Delivery Address',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
              maxLength: 20,
              controller: _addressController,
              decoration: InputDecoration(
                  labelText: 'Enter an addrress',
                  filled: true,
                  fillColor: const Color(0xFFCFDBD5),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none),
                  counterText: '',
                  errorStyle:
                      const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  isDense: true),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter valid address';
                } else if (value.length < 4) {
                  return 'Enter at least 4 characters';
                } else if (value.length > 50) {
                  return 'Enter maximum 50 characters';
                } else {
                  return null;
                }
              },
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field company phone
   */
  Widget buildTotal() => Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: const EdgeInsets.only(left: 55.0),
            child: const Text(
              'Purchase value',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
            ),
          ),
        ),
        const SizedBox(
          height: 5.0,
        ),
        SizedBox(
          width: 250.0,
          child: TextFormField(
              maxLength: 10,
              controller: _totalController,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp("[0-9.]")),
              ],
              decoration: InputDecoration(
                  filled: true,
                  fillColor: const Color(0xFFCFDBD5),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none),
                  labelText: 'Enter a value',
                  counterText: '',
                  errorStyle:
                      const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  isDense: true),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter a valid value';
                } else if (int.parse(value.trim()) < 0) {
                  return 'The value cannot be negative';
                } else if (!RegExp(
                        r"^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$")
                    .hasMatch(value)) {
                  return 'The value is not formatted correctly';
                } else {
                  return null;
                }
              }),
        )
      ]);

  Widget buildFunctionButtom() => ElevatedButton(
        onPressed: () async {
          final con = await _checkInternet();
          if (con.toString() == "true") {
            if (formKey.currentState!.validate()) {
              setState(() => isLoading = true);
              Orders act = widget.order;
              Orders ord = Orders(
                  id: act.id,
                  initialDate: act.initialDate,
                  arrivalDate: _dateController.text,
                  address: _addressController.text,
                  status: orderStatus,
                  total: _totalController.text,
                  shopId: act.shopId,
                  supplierId: act.supplierId);
              final receivePort = ReceivePort();
              await Isolate.spawn(
                _isolateService.updateOrder,
                [receivePort.sendPort, ord],
              );
              var res = await receivePort.first;
              setState(() => isLoading = false);
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const OrdersSuppliersPage()));


            }
          } else {
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      title: const Text('Network connection problem'),
                      content: const Text(
                          'The update functionality is not available since it is not connected to the WIFI/Mobile network.'),
                      backgroundColor: Colors.grey,
                      actions: [
                        TextButton(
                            child: const Text('Ok',
                                style: TextStyle(color: Colors.black)),
                            onPressed: () => Navigator.of(context).pop())
                      ],
                    ));
          }
        },
        child: isLoading
            ? SizedBox(
                width: 150,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SizedBox(
                      child: CircularProgressIndicator(
                          color: Colors.black, strokeWidth: 2),
                      height: 15.0,
                      width: 15.0,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Please Wait...',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: Colors.black),
                    )
                  ],
                ))
            : const Text(
                'Confirm',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: Colors.black),
              ),
        style: ElevatedButton.styleFrom(
          primary: const Color(0xFFCFDBD5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
      );
}