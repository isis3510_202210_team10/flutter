import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:in_shop_flutter/src/pages/sign_in.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/services/firebase_service.dart';
import 'package:flutter/services.dart';

import '../services/connectivity_service.dart';

class SignUpPage extends StatefulWidget {
  /*
   * String variable corresponding to the view 
   */
  static String id = 'sign_up';

  /*
   * Widget keys SignUpPage
   */
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

final ConnectivityService _connectivityService =
    GetIt.I.get<ConnectivityService>();

class _SignUpPageState extends State<SignUpPage> {
  /*
   * Initialize firebase instance for authentication
   */
  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();
    return firebaseApp;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
            future: _initializeFirebase(),
            builder: ((context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return const RegisterScreen();
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            })));
  }
}

class RegisterScreen extends StatefulWidget {
  /*
   * Screen register with its respective key
   */
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  /*
   * Firebase instance created for authentication with singleton pattern - Singleton
   */
  final FirebaseService _firebaseService = GetIt.I.get<FirebaseService>();

  /*
   * Registration form text field controllers
   */
  final TextEditingController _nameCompanyController = TextEditingController();
  final TextEditingController _emailCompanyController = TextEditingController();
  final TextEditingController _addressCompanyController =
      TextEditingController();
  final TextEditingController _phoneCompanyController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _repeatPasswordController =
      TextEditingController();

  /*
   * Variables corresponding to the form key and loading state
   */
  final formKey = GlobalKey<FormState>();
  bool isLoading = false;
  bool isSwitched = false;
  var textValue = "Shop";

  /*
   * Handling of the toggle switch to chose supplier or shop
   */
  void toggleSwitch(bool value) {
    if (isSwitched == false) {
      setState(() {
        isSwitched = true;
        textValue = "Supplier";
      });
    } else {
      setState(() {
        isSwitched = false;
        textValue = "Shop";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color(0xFF7A93AC),
        body: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 23.0),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 5.0,
                    ),
                    buildBanner(),
                    const SizedBox(
                      height: 15.0,
                    ),
                    buildCompanyName(),
                    const SizedBox(
                      height: 5.0,
                    ),
                    buildCompanyEmail(),
                    const SizedBox(
                      height: 5.0,
                    ),
                    buildCompanyAddress(),
                    const SizedBox(
                      height: 5.0,
                    ),
                    buildCompanyPhone(),
                    const SizedBox(
                      height: 5.0,
                    ),
                    buildPassword(),
                    const SizedBox(
                      height: 5.0,
                    ),
                    buildRepeatPassword(),
                    const SizedBox(
                      height: 15.0,
                    ),
                    buildUserType(),
                    const SizedBox(
                      height: 5.0,
                    ),
                    buildFunctionButtom(),
                    const SizedBox(
                      height: 15.0,
                    ),
                    buildFooter(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  /*
   * Function to check connectivity status in the view
   */
  Future<bool> _checkInternet() async {
    return _connectivityService.checkInternetConnection();
  }

  /*
   * Widget containing the elements that make up the banner
   */
  Widget buildBanner() => const Text(
        'Sign Up',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
      );

  /*
   * Widget containing the elements that make up the text field company name
   */
  Widget buildCompanyName() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Company name',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
              maxLength: 20,
              controller: _nameCompanyController,
              decoration: InputDecoration(
                  labelText: 'Company name',
                  filled: true,
                  fillColor: const Color(0xFFCFDBD5),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none),
                  counterText: '',
                  errorStyle:
                      const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  isDense: true),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter valid company name';
                } else if (value.length < 3) {
                  return 'Enter at least 3 characters';
                } else {
                  return null;
                }
              },
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field company email
   */
  Widget buildCompanyEmail() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Company email',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
                maxLength: 25,
                controller: _emailCompanyController,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: const Color(0xFFCFDBD5),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide.none),
                    labelText: 'Company email',
                    counterText: '',
                    errorStyle:
                        const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    isDense: true),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter valid company email';
                  } else if (!RegExp(
                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                      .hasMatch(value)) {
                    return 'Email is not formatted correctly';
                  } else {
                    return null;
                  }
                }),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field company address
   */
  Widget buildCompanyAddress() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Company address',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
                maxLength: 25,
                controller: _addressCompanyController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: const Color(0xFFCFDBD5),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide.none),
                    labelText: 'Company address',
                    counterText: '',
                    errorStyle:
                        const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    isDense: true),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter valid company address';
                  } else if (value.length < 10) {
                    return 'Enter at least 10 characters';
                  } else {
                    return null;
                  }
                }),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field company phone
   */
  Widget buildCompanyPhone() => Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: const EdgeInsets.only(left: 55.0),
            child: const Text(
              'Company phone',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
            ),
          ),
        ),
        const SizedBox(
          height: 5.0,
        ),
        SizedBox(
          width: 250.0,
          child: TextFormField(
              maxLength: 10,
              controller: _phoneCompanyController,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp("[0-9.]")),
              ],
              decoration: InputDecoration(
                  filled: true,
                  fillColor: const Color(0xFFCFDBD5),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none),
                  labelText: 'Company phone',
                  counterText: '',
                  errorStyle:
                      const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  isDense: true),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter valid company phone';
                } else if (value.length < 8) {
                  return 'Enter at least 8 characters';
                } else if (!RegExp(
                        r"^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$")
                    .hasMatch(value)) {
                  return 'Phone is not formatted correctly';
                } else {
                  return null;
                }
              }),
        )
      ]);

  /*
   * Widget containing the elements that make up the text field user type
   */
  Widget buildUserType() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Select your user type',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          SizedBox(
            width: 250,
            child: Row(
              children: [
                Switch(
                    onChanged: toggleSwitch,
                    value: isSwitched,
                    activeColor: Colors.black54,
                    activeTrackColor: const Color.fromARGB(255, 189, 189, 189),
                    inactiveThumbColor: Colors.black54,
                    inactiveTrackColor:
                        const Color.fromARGB(255, 189, 189, 189)),
                Text(textValue, style: const TextStyle(fontSize: 15))
              ],
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field password
   */
  Widget buildPassword() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Password',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          ConstrainedBox(
            constraints: const BoxConstraints.tightForFinite(width: 250),
            child: TextFormField(
                maxLength: 12,
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: const Color(0xFFCFDBD5),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide.none),
                    labelText: 'Password',
                    counterText: '',
                    errorStyle:
                        const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    isDense: true),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter valid password';
                  } else if (value.length < 5) {
                    return 'Enter at least 6 characters';
                  } else {
                    return null;
                  }
                }),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field repeat password
   */
  Widget buildRepeatPassword() => Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 55.0),
              child: const Text(
                'Repeat Password',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          SizedBox(
            width: 250.0,
            child: TextFormField(
                maxLength: 12,
                controller: _repeatPasswordController,
                obscureText: true,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: const Color(0xFFCFDBD5),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide.none),
                    labelText: 'Repeat Password',
                    counterText: '',
                    errorStyle:
                        const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    isDense: true),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter valid password';
                  } else if (value.length < 5) {
                    return 'Enter at least 6 characters';
                  } else {
                    return null;
                  }
                }),
          )
        ],
      );

  /*
   * Widget containing the elements that make up footer
   */
  Widget buildFooter() => Column(
        children: [
          const Text('Have account?', style: TextStyle(fontSize: 15)),
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const SignInPage()));
            },
            child: const Text(
              'Sign In',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                  decoration: TextDecoration.underline),
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the function for principal button
   */
  Widget buildFunctionButtom() => ElevatedButton(
        onPressed: () async {
          const urlShop = 'https://inshop-uniandes.herokuapp.com/shops';
          const urlSupplier = 'https://inshop-uniandes.herokuapp.com/suppliers';
          final con = await _checkInternet();
          if (con.toString() == "true") {
            if (formKey.currentState!.validate()) {
              if (textValue.toLowerCase() == 'shop') {
                if (_passwordController.text ==
                    _repeatPasswordController.text) {
                  setState(() => isLoading = true);
                  String signedUp = await _firebaseService.signUp(
                      _emailCompanyController.text, _passwordController.text);
                  var response = await http.post(Uri.parse(urlShop), body: {
                    "name": _nameCompanyController.text,
                    "email": _emailCompanyController.text,
                    "address": _addressCompanyController.text,
                    "phone": _phoneCompanyController.text,
                    "type": "FOOD"
                  });
                  if (signedUp == 'Signed up' &&
                      response.statusCode.toInt() == 200) {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => HomePage()));
                  } else {
                    setState(() => isLoading = false);
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              title: const Text('Sign Up error'),
                              content: const Text(
                                  'The email is already associated with an account. ¡Please use another email!'),
                              backgroundColor: Colors.grey,
                              actions: [
                                TextButton(
                                    child: const Text('Ok',
                                        style: TextStyle(color: Colors.black)),
                                    onPressed: () =>
                                        Navigator.of(context).pop())
                              ],
                            ));
                  }
                } else {
                  setState(() => isLoading = false);
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: const Text('Sign Up error'),
                            content: const Text(
                                'Password and repeat password do not match. ¡Please check!'),
                            backgroundColor: Colors.grey,
                            actions: [
                              TextButton(
                                  child: const Text('Ok',
                                      style: TextStyle(color: Colors.black)),
                                  onPressed: () => Navigator.of(context).pop())
                            ],
                          ));
                }
              } else if (textValue.toLowerCase() == 'supplier') {
                if (_passwordController.text ==
                    _repeatPasswordController.text) {
                  setState(() => isLoading = true);
                  String signedUp = await _firebaseService.signUp(
                      _emailCompanyController.text, _passwordController.text);
                  var response = await http.post(Uri.parse(urlSupplier), body: {
                    "name": _nameCompanyController.text,
                    "email": _emailCompanyController.text,
                    "address": _addressCompanyController.text,
                    "phone": _phoneCompanyController.text,
                    "companyName": "Compania1"
                  });
                  if (signedUp == 'Signed up' &&
                      response.statusCode.toInt() == 200) {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => HomePage()));
                  }
                } else {
                  setState(() => isLoading = false);
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: const Text('Sign Up error'),
                            content: const Text(
                                'Password and repeat password do not match'),
                            backgroundColor: Colors.grey,
                            actions: [
                              TextButton(
                                  child: const Text('Ok',
                                      style: TextStyle(color: Colors.black)),
                                  onPressed: () => Navigator.of(context).pop())
                            ],
                          ));
                }
              }
            }
          } else {
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      title: const Text('Network connection problem'),
                      content: const Text(
                          'The registration functionality is not available since it is not connected to the WIFI/Mobile network.'),
                      backgroundColor: Colors.grey,
                      actions: [
                        TextButton(
                            child: const Text('Ok',
                                style: TextStyle(color: Colors.black)),
                            onPressed: () => Navigator.of(context).pop())
                      ],
                    ));
          }
        },
        child: isLoading
            ? SizedBox(
                width: 150,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SizedBox(
                      child: CircularProgressIndicator(
                          color: Colors.black, strokeWidth: 2),
                      height: 15.0,
                      width: 15.0,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Please Wait...',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: Colors.black),
                    )
                  ],
                ))
            : const Text(
                'Sign Up',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: Colors.black),
              ),
        style: ElevatedButton.styleFrom(
          primary: const Color(0xFFCFDBD5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
      );
}
