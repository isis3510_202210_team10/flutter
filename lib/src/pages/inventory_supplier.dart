import 'dart:isolate';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';

import 'package:in_shop_flutter/src/models/supplier_product.dart';
import 'package:in_shop_flutter/src/database/suppiler_product_database.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';

import '../services/isolate_service.dart';
import '../services/shared_prefs_service.dart';
import '../services/sqlite_service.dart';
import 'orders_suppliers.dart';

class InventorySupplierPage extends StatefulWidget {
  static String id = 'InventorySupplierPage';

  @override
  State<InventorySupplierPage> createState() => _InventorySupplierPageState();
}

class _InventorySupplierPageState extends State<InventorySupplierPage> {
  List<SupplierProduct> products = [];
  int sumatotal = 0;
  bool isLoading = false;
  final items = ['Profile', 'Log out'];

  late SupplierProductDatabase database;
  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  @override
  void initState() {
    super.initState();
    userData();
    $FloorSupplierProductDatabase
        .databaseBuilder('suppiler_product_database.db')
        .build()
        .then((value) async {
      database = value;
      // get products from local db
      List<SupplierProduct> prueba =
          await _sqliteService.retrieveSupplierProducts(database);
      setState(() {
        products = prueba;
      });
      // get producs from api to update changes in local db
      List<SupplierProduct> tpmProducts = await fetchProducts();
      if (mounted) {
        setState(() {
          products = tpmProducts;
        });
        await _sqliteService.deleteSupplierProducts(database); // clean local db
        await _sqliteService.addSupplierProducts(
            database, products); // save in local db
      }
    });
  }

  final IsolateService _isolateService = GetIt.I.get<IsolateService>();

  final SqliteService _sqliteService = GetIt.I.get<SqliteService>();

  fetchProducts() async {
    final receivePort = ReceivePort();
    String id = await _sharedPreferencesService.getStringValue("id");
    await Isolate.spawn(
      _isolateService.fetchSupplierProducts,
      [receivePort.sendPort, id],
    );
    return await receivePort.first;
  }

  final ScrollController _mycontroller = ScrollController();
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text(
          "Inventory",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: const [
          CustomBuilder(),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      endDrawer: CustomDrawer(inventory: false),
      body: Container(
        padding: const EdgeInsets.all(17),
        color: const Color(0xFFE5E5E5),
        child: Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(25.0),
                  bottomRight: Radius.circular(25.0),
                  topLeft: Radius.circular(25.0),
                  bottomLeft: Radius.circular(25.0)),
              color: Color(0xFF7A93AC)),
          child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.13,
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25.0),
                    bottomRight: Radius.circular(25.0),
                    topLeft: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                  ),
                ),
                child: const Text(
                  "Food Products",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 0),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 0),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height * 0.33,
                    child: Expanded(
                      child: Container(
                        color: const Color(0xFF7A93AC),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.065,
                              right: MediaQuery.of(context).size.width * 0.04,
                              bottom: 25.0),
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.78,
                                decoration: const BoxDecoration(
                                    color: Color(0xFFCFDBD5),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0),
                                      bottomLeft: Radius.circular(15.0),
                                      bottomRight: Radius.circular(15.0),
                                    )),
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  controller: _mycontroller,
                                  child: DataTable(
                                    columns: const [
                                      DataColumn(label: Text('ID')),
                                      DataColumn(label: Text('Name')),
                                      DataColumn(label: Text('Type')),
                                      DataColumn(label: Text('Exp')),
                                      DataColumn(label: Text('Stock')),
                                    ],
                                    rows: products
                                        .map(
                                          (emp) => DataRow(cells: [
                                            DataCell(
                                              Text(emp.id.substring(
                                                  emp.id.length - 4)),
                                            ),
                                            DataCell(
                                              Text(emp.name),
                                            ),
                                            DataCell(
                                              Text(emp.type),
                                            ),
                                            DataCell(
                                              Text(emp.expirationDate),
                                            ),
                                            DataCell(
                                              Text(emp.stock),
                                            ),
                                          ]),
                                        )
                                        .toList(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                  height: 25,
                  margin: const EdgeInsets.only(bottom: 15),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2, // 20%
                        child: Container(
                          color: const Color(0xFF7A93AC),
                        ),
                      ),
                      Expanded(
                        flex: 3, // 60%
                        child: Container(
                            padding: const EdgeInsets.only(left: 20),
                            alignment: Alignment.topCenter,
                            color: const Color(0xFF7A93AC),
                            child: const Text(
                              "Total:",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            )),
                      ),
                      Expanded(
                        flex: 2, // 20%
                        child: Container(
                          alignment: Alignment.topLeft,
                          padding: const EdgeInsets.only(left: 15),
                          color: const Color(0xFF7A93AC),
                          child: Text(
                            sumatotal.toString(),
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ],
                  )),
              Container(
                  color: const Color(0xFF7A93AC),
                  child: Column(children: [
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: const Color(0xFF7A93AC),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: ElevatedButton.icon(
                            icon: const ImageIcon(
                              AssetImage("images/plus.png"),
                              color: Colors.black,
                            ),
                            label: const Text(
                              'Add product      ',
                              style: TextStyle(color: Colors.black),
                            ),
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                              primary: const Color(0xFFD5A021),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: const Color(0xFF7A93AC),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: const Color(0xFF7A93AC),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: ElevatedButton.icon(
                            icon: const ImageIcon(
                              AssetImage("images/update-arrow.png"),
                            ),
                            label: const Text('Update product'),
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                              primary: const Color(0xFF243B4A),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: const Color(0xFF7A93AC),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: const Color(0xFF7A93AC),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: ElevatedButton.icon(
                            icon: const ImageIcon(
                              AssetImage(
                                "images/close.png",
                              ),
                              size: 20,
                            ),
                            label: const Text('Delete product'),
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                              primary: const Color(0xFF243B4A),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: const Color(0xFF7A93AC),
                          ),
                        ),
                      ],
                    ),
                  ]))
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        currentIndex: index,
        onTap: (int index) {
          setState(() {
            this.index = index;
          });
          if (index == 0) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => HomePage()));
          }
          if (index == 1) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => (type == "suppliers")
                    ? const OrdersSuppliersPage()
                    : const OrderShopsPage()));
          }
          if (index == 2) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
          if (index == 3) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
        },
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/store.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/list.png"),
                color: Color(0xFF000000),
                size: 43,
              ),
              label: "Inventory"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/bar-graph.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Dashboard"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/gear.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Settings"),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
    ));
  }
}
