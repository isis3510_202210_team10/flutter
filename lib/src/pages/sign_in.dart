import 'dart:async';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:in_shop_flutter/src/classes/user_sigin.dart';
import 'package:in_shop_flutter/src/pages/connectivity_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/sign_up.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/services/firebase_service.dart';
import 'package:http/http.dart' as http;
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../services/connectivity_service.dart';

class SignInPage extends StatefulWidget {
  /*
   * String variable corresponding to the view 
   */
  static String id = 'sing_in';

  /*
   * Widget keys SignInPage
   */
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  /*
   * Variables for connectivity management
   */

  // conectivity
  bool isConected = true;
  bool firstTime = true;

  Stream<bool> getConnection() async* {
    while (true) {
      await Future.delayed(const Duration(milliseconds: 500));
      yield await _connectivityService.checkInternetConnection();
    }
  }

  /*
   * Initialize firebase instance for authentication
   */
  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();
    return firebaseApp;
  }

  final ConnectivityService _connectivityService =
      GetIt.I.get<ConnectivityService>();

  /*
   * InitState for connectivity management
   */
  @override
  initState() {
    super.initState();
    getConnection().listen((value) {
      if (mounted) {
        if (!isConected && value) {
          setState(() {
            isConected = true;
          });
        } else if (!value && isConected) {
          setState(() {
            isConected = false;
          });
        }
      }
    });
  }

  /*
   * Dispose for connectivity management
   */
  @override
  dispose() {
    super.dispose();
  }

  /*
   * Function to check connectivity status in the view
   */
  // ignore: unused_element
  Future<bool> _checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isConected
            ? FutureBuilder(
                future: _initializeFirebase(),
                builder: ((context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return const LoginScreen();
                  }
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }))
            : ConnectivityPage(connectivity: isConected));
  }
}

class LoginScreen extends StatefulWidget {
  /*
   * Screen register with its respective key
   */
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  /*
   * Firebase instance created for authentication with singleton pattern - Singleton
   */
  final FirebaseService _firebaseService = GetIt.I.get<FirebaseService>();

  /*
   * Login form text field controllers
   */
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  /*
   * Variables corresponding to the form key and loading state
   */
  final formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: const Color(0xFF7A93AC),
          body: SingleChildScrollView(
              child: Form(
            key: formKey,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Column(
                  children: [
                    buildBanner(),
                    const SizedBox(
                      height: 23.0,
                    ),
                    buildEmailShopOrSupplier(),
                    const SizedBox(
                      height: 13.0,
                    ),
                    buildPassword(),
                    const SizedBox(
                      height: 15.0,
                    ),
                    buildConectionComponent1(),
                    const SizedBox(
                      height: 10.0,
                    ),
                    buildFunctionButtom(),
                    const SizedBox(
                      height: 10.0,
                    ),
                    buildConectionComponent2(),
                    const SizedBox(
                      height: 10.0,
                    ),
                    buildButtonsExternalSignIn(),
                    const SizedBox(
                      height: 18.0,
                    ),
                    buildFooter()
                  ],
                ),
              ),
            ),
          ))),
    );
  }

  /*
   * Save id and type of user in shared preferences
   */
  // ignore: non_constant_identifier_names
  Future<void> save_sharedPrefereces(id, typeUser, name, type) async {
    final prefs = await SharedPreferences.getInstance();

    // Save an String value to 'id' key.
    await prefs.setString('id', id);
    // Save an String value to 'typeUser' key.
    await prefs.setString('typeUser', typeUser);
    // Save an String value to 'typeUser' key.
    await prefs.setString('name', name);
    // Save an String value to 'typeUser' key.
    await prefs.setString('type', type);
  }

  /*
   * Widget containing the elements that make up the banner
   */
  Widget buildBanner() => Column(
        children: [
          Image.asset(
            'images/logo.png',
            height: 130.0,
          ),
          const SizedBox(
            height: 25.0,
          ),
          const Text(
            'Sign In',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
          )
        ],
      );

  /*
   * Widget containing the elements that make up the text field email shop or supplier
   */
  Widget buildEmailShopOrSupplier() => SizedBox(
        width: 250.0,
        child: TextFormField(
            maxLength: 30,
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            enableSuggestions: false,
            autocorrect: false,
            style: const TextStyle(
              decoration: TextDecoration.none,
              color: Colors.black,
            ),
            decoration: InputDecoration(
                filled: true,
                fillColor: const Color(0xFFCFDBD5),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide.none),
                labelText: 'Email',
                counterText: '',
                errorStyle:
                    const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
                floatingLabelBehavior: FloatingLabelBehavior.never,
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                isDense: true),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Enter valid company email';
              } else if (!RegExp(
                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                  .hasMatch(value)) {
                return 'Email is not formatted correctly';
              } else {
                return null;
              }
            }),
      );

  /*
   * Widget containing the elements that make up the text field password
   */
  Widget buildPassword() => SizedBox(
        width: 250.0,
        child: TextFormField(
          maxLength: 20,
          controller: _passwordController,
          obscureText: true,
          decoration: InputDecoration(
              filled: true,
              fillColor: const Color(0xFFCFDBD5),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide.none),
              labelText: 'Password',
              counterText: '',
              errorStyle:
                  const TextStyle(color: Color.fromARGB(255, 163, 4, 4)),
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              isDense: true),
          validator: (value) {
            if (value!.isEmpty) {
              return 'Enter valid password';
            } else if (value.length < 5) {
              return 'Enter at least 6 characters';
            } else {
              return null;
            }
          },
        ),
      );

  /*
   * Widget containing the elements that make up conection component 1
   */
  Widget buildConectionComponent1() => InkWell(
        onTap: () {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: const Text('Change of password'),
                    content: const Text(
                        'To change your user\'s password, please contact: \n -> inshop@gmail.com'),
                    backgroundColor: Colors.grey,
                    actions: [
                      TextButton(
                          child: const Text('Ok',
                              style: TextStyle(color: Colors.black)),
                          onPressed: () => Navigator.of(context).pop())
                    ],
                  ));
        },
        child: const Text(
          'Forgot password?',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              decoration: TextDecoration.underline),
        ),
      );

  /*
   * Widget containing the elements that make up the function for principal button
   */
  Widget buildFunctionButtom() => ElevatedButton(
        onPressed: () async {
          if (formKey.currentState!.validate()) {
            setState(() => isLoading = true);
            User? user = await _firebaseService.signIn(
                _emailController.text, _passwordController.text);
            UserSignIn? userSignIn = await getIdUser(_emailController.text);
            print(user);
            print("Email" + _emailController.text);
            print("Password" + _passwordController.text);
            if (user != null && userSignIn != null) {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => HomePage(userType: userSignIn)));
              setState(() => isLoading = false);
            } else {
              setState(() => isLoading = false);
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        title: const Text('Sign In error'),
                        content: const Text(
                            'The email or password is not correct. ¡Please check!'),
                        backgroundColor: Colors.grey,
                        actions: [
                          TextButton(
                              child: const Text('Ok',
                                  style: TextStyle(color: Colors.black)),
                              onPressed: () => Navigator.of(context).pop())
                        ],
                      ));
            }
          }
        },
        child: isLoading
            ? SizedBox(
                width: 150,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SizedBox(
                      child: CircularProgressIndicator(
                          color: Colors.black, strokeWidth: 2),
                      height: 15.0,
                      width: 15.0,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Please Wait...',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: Colors.black),
                    )
                  ],
                ))
            : const Text(
                'Sign In',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: Colors.black),
              ),
        style: ElevatedButton.styleFrom(
          primary: const Color(0xFFCFDBD5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
      );

  /*
   * Widget containing the elements that make up conection component 1
   */
  Widget buildConectionComponent2() => const Text(
        'Or',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
      );

  /*
   * Widget containing the elements that make up the buttons for external sign in
   */
  Widget buildButtonsExternalSignIn() => Column(
        children: [
          SizedBox(
            width: 140.0,
            child: ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          title: const Text('In development'),
                          content: const Text(
                              'Authentication with Google will be available soon.'),
                          backgroundColor: Colors.grey,
                          actions: [
                            TextButton(
                                child: const Text('Ok',
                                    style: TextStyle(color: Colors.black)),
                                onPressed: () => Navigator.of(context).pop())
                          ],
                        ));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.asset('images/google.png'),
                  const Text('Google',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: Colors.white)),
                ],
              ),
              style: ElevatedButton.styleFrom(
                primary: const Color(0xFF243B4A),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 140.0,
            child: ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          title: const Text('In development'),
                          content: const Text(
                              'Authentication with Microsoft will be available soon.'),
                          backgroundColor: Colors.grey,
                          actions: [
                            TextButton(
                                child: const Text('Ok',
                                    style: TextStyle(color: Colors.black)),
                                onPressed: () => Navigator.of(context).pop())
                          ],
                        ));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.asset('images/microsoft.png'),
                  const Text('Microsoft',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: Colors.white)),
                ],
              ),
              style: ElevatedButton.styleFrom(
                primary: const Color(0xFF243B4A),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
            ),
          )
        ],
      );

  /*
   * Widget containing the elements that make up footer
   */
  Widget buildFooter() => Column(
        children: [
          const Text('Don’t have account?', style: TextStyle(fontSize: 15)),
          InkWell(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => SignUpPage()));
            },
            child: const Text(
              'Create Account',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                  decoration: TextDecoration.underline),
            ),
          ),
        ],
      );

  /*
   * Function that obtains the type of user and the corresponding user id
   */
  Future<UserSignIn?> getIdUser(email) async {
    String urlShop =
        'https://inshop-uniandes.herokuapp.com/shops/email/' + email;
    String urlSupplier =
        'https://inshop-uniandes.herokuapp.com/suppliers/email/' + email;

    try {
      var responseShop = await http.get(Uri.parse(urlShop));
      var responseSupplier = await http.get(Uri.parse(urlSupplier));

      if (responseSupplier.body.toString() == '') {
        await save_sharedPrefereces(
            jsonDecode(responseShop.body)['_id'].toString(),
            'shops',
            jsonDecode(responseShop.body)['name'].toString(),
            jsonDecode(responseShop.body)['type'].toString());
        return UserSignIn(
            'shops', jsonDecode(responseShop.body)['_id'].toString());
      } else if (responseShop.body.toString() == '') {
        await save_sharedPrefereces(
            jsonDecode(responseSupplier.body)['_id'].toString(),
            'suppliers',
            jsonDecode(responseSupplier.body)['name'].toString(),
            jsonDecode(responseSupplier.body)['companyName'].toString());
        return UserSignIn(
            'suppliers', jsonDecode(responseSupplier.body)['_id'].toString());
      } else {
        return Future.error("Server Error");
      }
    } catch (e) {
      return Future.error(e);
    }
  }
}
