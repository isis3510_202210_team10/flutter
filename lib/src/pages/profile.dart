import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_it/get_it.dart';
import 'dart:convert';
import "package:http/http.dart" as http;
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:in_shop_flutter/src/widgets/custom_bottom_navigation_bar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:url_launcher/url_launcher.dart';

import '../services/shared_prefs_service.dart';

class ProfilePage extends StatefulWidget {
  /*
   * String variable corresponding to the view 
   */
  static String id = 'ProfilePage';

  /*
   * Widget keys SignInPage
   */
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  /*
   * User information variables
   */
  String name = "";
  String type = "";
  String address = "";
  int dirImage = 0;
  String termsAndContidions = "";

  /*
   * _image and _picker variables
   */
  File? _image;
  final _picker = ImagePicker();

  /*
   * Url of where the network image is located
   */
  String url =
      'https://cdn.discordapp.com/attachments/807264106474111017/980349041319751700/Foto_Perfil.jpeg';

  /*
   * The flutter cache manager library is used to obtain the image of a profile found on the network
   */
  imageFile() async {
    var fetchedFile = await DefaultCacheManager().getSingleFile(url);
    return fetchedFile;
  }

  /*
   * SharedPreferences variables
   */
  late SharedPreferences prefs;
  final _gKey = GlobalKey<ScaffoldState>();

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  /*
   * InitState for loadPhoto and fetchUser
   */
  @override
  void initState() {
    super.initState();
    loadPhoto();
    fetchUser();
  }

  /*
   * Obtain user information using the cache, falling to network strategy.
   */
  fetchUser() async {
    // Get user id and user type from SharedPreferences
    String? id = await _sharedPreferencesService.getStringValue("id");
    String? typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');

    // Url where the resource is defined to obtain the user's information
    var url = "https://inshop-uniandes.herokuapp.com/" + typeUser + "/" + id;

    // Creation of the json file to store user information
    String fileName = "UserInfo.json";
    var dir = await getTemporaryDirectory();
    File file = File(dir.path + "/" + fileName);

    // Creation of the json file to store user information
    String fileTerms = "terms.txt";
    File terms = File(dir.path + "/" + fileTerms);

    if (terms.existsSync()) {
      // Read cache file information
      final data = terms.readAsStringSync();
      setState(() {
        termsAndContidions = data;
      });
    } else {
      String contenido =
          "You must be at least 13 years of age to use our InShop application. By using our InShop application and by agreeing to these terms of use, you warrant and represent that you are not less than 13 years of age.";

      try {
        terms.writeAsStringSync(contenido, flush: true, mode: FileMode.write);
        setState(() {
          termsAndContidions = contenido;
        });
      } catch (e) {
        // ignore: avoid_print
        print(e);
      }
    }

    if (file.existsSync()) {
      // Read cache file information
      final data = file.readAsStringSync();
      final res = json.decode(data);
      setState(() {
        name = res["name"];
        type = (typeUser == 'suppliers') ? res["companyName"] : res["type"];
        address = res["address"];
      });
    } else {
      // Obtain API user information
      var response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        try {
          var item = json.decode(response.body);
          file.writeAsStringSync(response.body,
              flush: true, mode: FileMode.write);
          setState(() {
            name = item["name"];
            type =
                (typeUser == 'suppliers') ? item["companyName"] : item["type"];
            address = item["address"];
          });
        } catch (e) {
          // ignore: avoid_print
          print(e);
        }
      } else {
        name = "";
        type = "";
        address = "";
      }
    }
  }

  /*
   * Load profile picture in the view
   */
  loadPhoto() async {
    prefs = await SharedPreferences.getInstance();
    try {
      String? profilePath = prefs.getString("profile_image");
      setState(() {
        _image = File(profilePath!);
      });
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  /*
   * Open the image selector tool
   */
  Future<void> _openImagePicker(method) async {
    // Escoger la imagen
    final XFile? pickedImage = await _picker.pickImage(source: method);

    // Convertir la imagen a file
    if (pickedImage != null) {
      File selectedImage = File(pickedImage.path);

      // Obtener directorio de la app para guardar la imagen
      Directory appDocDir = await getApplicationDocumentsDirectory();
      String path = appDocDir.path;

      setState(() {
        _image = File(selectedImage.path);
      });

      File? localImage = await _image?.copy('$path/profile.png');
      prefs.setString('profile_image', localImage!.path);
    }
  }

  /*
   * Launching the function to enable the call option
   */
  void _launchUrl(_url) async {
    // ignore: deprecated_member_use
    if (!await launch(_url)) throw 'Could not launch $_url';
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      key: _gKey,
      appBar: AppBar(
        title: Text(
          "Profile",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        backgroundColor: const Color(0xFFD5A021),
      ),
      body: Stack(
        children: <Widget>[
          // ignore: avoid_unnecessary_containers
          Container(
            child: Card(
              margin: EdgeInsets.only(
                  top: 0.0,
                  bottom: MediaQuery.of(context).size.height * 0.37,
                  left: 0.0,
                  right: 0.0),
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: const RoundedRectangleBorder(),
              color: const Color(0xFF7A93AC),
              child: Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.015),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        CircleAvatar(
                          radius: MediaQuery.of(context).size.width /
                              5.3, // Image radius
                          backgroundImage: _image == null
                              ? const AssetImage("images/user.png")
                                  as ImageProvider
                              : FileImage(_image!),
                        ),
                        Positioned(
                          right: 0,
                          top: 0,
                          child: IconButton(
                            onPressed: () {
                              showModalBottomSheet<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return Stack(
                                    children: <Widget>[
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.215,
                                        color: const Color(0xFF7A93AC),
                                      ),
                                      Positioned(
                                        top: 13,
                                        left: 13,
                                        child: Text(
                                          "Profile picture",
                                          style: TextStyle(
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.025,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.08,
                                        left: 13,
                                        child: IconButton(
                                            icon: const Icon(
                                                Icons.camera_alt_rounded),
                                            iconSize: 30,
                                            onPressed: () {
                                              _openImagePicker(
                                                  ImageSource.camera);
                                            }),
                                      ),
                                      Positioned(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.08,
                                        left: 100,
                                        child: IconButton(
                                            icon: const Icon(Icons.image),
                                            iconSize: 30,
                                            onPressed: () {
                                              _openImagePicker(
                                                  ImageSource.gallery);
                                            }),
                                      ),
                                      Positioned(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.135,
                                          left: 12,
                                          child: const Text("Camera")),
                                      Positioned(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.135,
                                        left: 103,
                                        child: const Text("Galery"),
                                      ),
                                      Positioned(
                                        top: 0,
                                        right: 0,
                                        child: IconButton(
                                          icon: const Icon(Icons.close),
                                          iconSize: 23,
                                          onPressed: () =>
                                              Navigator.pop(context),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                            icon: Icon(
                              Icons.camera_alt,
                              color: Colors.black,
                              size: MediaQuery.of(context).size.height * 0.042,
                            ),
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Center(
                        child: Text(
                          name,
                          style: TextStyle(
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.030,
                              color: const Color(0xFF3E2723)),
                        ),
                      ),
                      subtitle: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: Text(
                              type,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: MediaQuery.of(context).size.height *
                                      0.028,
                                  decoration: TextDecoration.underline),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: 10.0,
                                bottom:
                                    MediaQuery.of(context).size.height * 0.01),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.22,
                                  ),
                                  Image.asset(
                                    'images/location.png',
                                    height: MediaQuery.of(context).size.height *
                                        0.033,
                                    width: MediaQuery.of(context).size.height *
                                        0.035,
                                  ),
                                  Expanded(
                                    child: Text(
                                      address,
                                      //overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        // overflow: TextOverflow.ellipsis,
                                        color: Colors.black,
                                        fontSize:
                                            MediaQuery.of(context).size.height *
                                                0.026,
                                      ),
                                    ),
                                  ),
                                ]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.305,
                bottom: MediaQuery.of(context).size.height * 0.01),
            child: ButtonBar(
              alignment: MainAxisAlignment.center,
              buttonPadding: const EdgeInsets.symmetric(
                horizontal: 35,
              ),
              children: <Widget>[
                CircleAvatar(
                  child: IconButton(
                    icon: Image.asset(
                      'images/telephone-call.png',
                      width: 38,
                      height: 38,
                    ),
                    color: Colors.white,
                    onPressed: () {
                      _launchUrl("tel://+573222102331");
                    },
                  ),
                  radius: MediaQuery.of(context).size.width * 0.088,
                  backgroundColor: const Color(0xFFD5A021),
                ),
                CircleAvatar(
                  child: Image.asset(
                    'images/chat.png',
                    width: 38,
                    height: 38,
                  ),
                  radius: MediaQuery.of(context).size.width * 0.088,
                  backgroundColor: const Color(0xFFD5A021),
                ),
                CircleAvatar(
                  child: Image.asset(
                    'images/information-button.png',
                    width: 38,
                    height: 38,
                  ),
                  radius: MediaQuery.of(context).size.width * 0.088,
                  backgroundColor: const Color(0xFFD5A021),
                ),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.425,
                  bottom: MediaQuery.of(context).size.height * 0.03),
              child: ListView(
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.all(30),
                itemExtent: 70.0,
                shrinkWrap: true,
                children: [
                  Center(
                    child: ListTile(
                      title: const Text(
                        'Company Information',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                      trailing: Image.asset(
                        'images/next.png',
                        height: MediaQuery.of(context).size.height * 0.040,
                        width: MediaQuery.of(context).size.height * 0.040,
                      ),
                    ),
                  ),
                  Center(
                    child: ListTile(
                      title: const Text(
                        'Legal Aspects',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                      trailing: Image.asset(
                        'images/next.png',
                        height: MediaQuery.of(context).size.height * 0.040,
                        width: MediaQuery.of(context).size.height * 0.040,
                      ),
                    ),
                  ),
                  Center(
                    child: ListTile(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                                  title: const Text('Terms & Conditions'),
                                  content: Text(termsAndContidions),
                                  backgroundColor: Colors.white,
                                  actions: [
                                    TextButton(
                                        child: const Text('Ok',
                                            style:
                                                TextStyle(color: Colors.black)),
                                        onPressed: () =>
                                            Navigator.of(context).pop())
                                  ],
                                ));
                      },
                      title: const Center(
                        child: Text(
                          'Terms & Conditions',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
      bottomNavigationBar: const CustomBottomNavigationBar(),
    ));
  }

  /*
   * Display secondary tools menu to take profile picture or select a previous picture
   */
  void displayBottomSheet() {
    _gKey.currentState?.showBottomSheet((context) {
      return Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.215,
            color: const Color(0xFF7A93AC),
          ),
          Positioned(
            top: 13,
            left: 13,
            child: Text(
              "Profile picture",
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.height * 0.025,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.08,
            left: 13,
            child: IconButton(
                icon: const Icon(Icons.camera_alt_rounded),
                iconSize: 30,
                onPressed: () {
                  print("XD");
                }),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.08,
            left: 100,
            child: IconButton(
                icon: const Icon(Icons.image),
                iconSize: 30,
                onPressed: () {
                  print("XD");
                }),
          ),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.135,
              left: 12,
              child: const Text("Camera")),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.135,
            left: 103,
            child: const Text("Galery"),
          ),
        ],
      );
    });
  }
}
