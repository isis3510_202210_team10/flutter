import 'dart:async';
import 'dart:isolate';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/pages/dasboard.dart';

import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';
import 'package:in_shop_flutter/src/models/shop_product.dart';
import 'package:in_shop_flutter/src/database/shop_product_database.dart';
import 'package:in_shop_flutter/src/pages/update_product.dart';
import 'package:in_shop_flutter/src/services/isolate_service.dart';
import 'package:in_shop_flutter/src/services/sqlite_service.dart';
import '../services/connectivity_service.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../services/shared_prefs_service.dart';
import 'orders_suppliers.dart';

class InventoryShopPage extends StatefulWidget {
  static String id = 'InventoryShopPage';

  const InventoryShopPage({Key? key}) : super(key: key);

  @override
  State<InventoryShopPage> createState() => _InventoryShopPageState();
}

class _InventoryShopPageState extends State<InventoryShopPage> {
  List<ShopProduct> products = [];
  String precioTotal = "0";
  String foodPrice = "0";
  String cleaningPrice = "0";
  String technologyPrice = "0";
  String clothingPrice = "0";
  String stationeryPrice = "0";
  String stockTotal = "0";
  String foodTotal = "0";
  String cleaningTotal = "0";
  String technologyTotal = "0";
  String clothingTotal = "0";
  String stationeryTotal = "0";
  bool isLoading = false;
  final items = ['Profile', 'Log out'];

  late ShopProductDatabase database;

  // conectivity
  bool isConected = true;
  bool firstTime = true;

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  final ConnectivityService _connectivityService =
      GetIt.I.get<ConnectivityService>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  Stream<bool> getConnection() async* {
    while (true) {
      await Future.delayed(const Duration(milliseconds: 500));
      yield await _connectivityService.checkInternetConnection();
    }
  }

  @override
  void initState() {
    super.initState();
    userData();
    getConnection().listen((value) {
      if (mounted) {
        setState(() {
          if (!isConected && value || firstTime && value) {
            print("Buenos días");
            fetchState();
            isConected = true;
            firstTime = false;
            print("XDDDDD");
          } else if (!value) {
            isConected = false;
          } else {
            isConected = true;
          }
        });
      }
    });
    $FloorShopProductDatabase
        .databaseBuilder('shop_product_database.db')
        .build()
        .then((value) async {
      database = value;
      // get products from local db
      List<ShopProduct> prueba =
          await _sqliteService.retrieveShopProducts(database);
      setState(() {
        products = prueba;
      });
    });
  }

  fetchState() async {
    List<ShopProduct> tpmProducts = await fetchProducts();
    if (mounted) {
      print("SI SEÑOR");
      setState(() {
        products = tpmProducts;
      });
      await _sqliteService.deleteShopProducts(database); // clean local db
      await _sqliteService.addShopProducts(
          database, products); // save in local db
    }
  }

  /*
   * Dispose for connectivity management
   */
  @override
  dispose() {
    super.dispose();
  }

  final IsolateService _isolateService = GetIt.I.get<IsolateService>();

  final SqliteService _sqliteService = GetIt.I.get<SqliteService>();

  fetchProducts() async {
    final receivePort = ReceivePort();
    String id = await _sharedPreferencesService.getStringValue("id");
    await Isolate.spawn(
      _isolateService.fetchShopProducts,
      [receivePort.sendPort, id],
    );
    return await receivePort.first;
  }

  final ScrollController _mycontroller = ScrollController();
  int index = 0;

  String dateToday = "Last update:" +
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day,
              DateTime.now().hour, DateTime.now().minute)
          .toString()
          .substring(0, 16);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text(
          "Inventory",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: const [
          CustomBuilder(),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      endDrawer: CustomDrawer(inventory: false),
      body:
          //? ConnectivityPage(connectivity: isConected) :
          SingleChildScrollView(
        child: Column(
          children: <Widget>[
            !isConected
                ? IntrinsicWidth(
                    child: Container(
                      height: 25,
                      width: MediaQuery.of(context).size.width,
                      color: const Color(0xFF7A93AC),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: const [
                          Text("No internet connection"),
                        ],
                      ),
                    ),
                  )
                : Container(),
            !isConected && products.isEmpty
                ? Column(
                    children: [
                      Image.asset(
                        'images/wifi-off.png',
                      ),
                      const SizedBox(
                        height: 15.0,
                      ),
                      const Text(
                        'No internet connection',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Color(0xFFCFDBD5)),
                      ),
                      const SizedBox(
                        height: 6.0,
                      ),
                      const Text(
                        'Check your internet connection status.',
                        style:
                            TextStyle(fontSize: 15, color: Color(0xFFCFDBD5)),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                    ],
                  )
                : Container(
                    padding: const EdgeInsets.all(17),
                    color: const Color(0xFFE5E5E5),
                    child: Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(25.0),
                              bottomRight: Radius.circular(25.0),
                              topLeft: Radius.circular(25.0),
                              bottomLeft: Radius.circular(25.0)),
                          color: Color(0xFF7A93AC)),
                      child: Column(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * 0.13,
                            alignment: Alignment.center,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(25.0),
                                bottomRight: Radius.circular(25.0),
                                topLeft: Radius.circular(25.0),
                                bottomLeft: Radius.circular(25.0),
                              ),
                            ),
                            child: Column(children: [
                              const SizedBox(
                                height: 15,
                              ),
                              const Text(
                                "Products",
                                style: TextStyle(
                                    fontSize: 22, fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Text(
                                dateToday,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    decoration: TextDecoration.underline),
                              )
                            ]),
                          ),
                          Container(
                            margin: const EdgeInsets.only(bottom: 0),
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 0),
                              child: Container(
                                color: const Color(0xFF7A93AC),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.065,
                                      right: MediaQuery.of(context).size.width *
                                          0.04,
                                      bottom: 25.0),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.78,
                                        decoration: const BoxDecoration(
                                            color: Color(0xFFCFDBD5),
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(15.0),
                                              topRight: Radius.circular(15.0),
                                              bottomLeft: Radius.circular(15.0),
                                              bottomRight:
                                                  Radius.circular(15.0),
                                            )),
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          controller: _mycontroller,
                                          child: DataTable(
                                            columns: const [
                                              DataColumn(label: Text('ID')),
                                              DataColumn(label: Text('Name')),
                                              DataColumn(label: Text('Type')),
                                              DataColumn(
                                                  label:
                                                      Text('Expiration Date')),
                                              DataColumn(
                                                  label:
                                                      Text('Price per unit')),
                                              DataColumn(label: Text('Stock')),
                                            ],
                                            rows: products
                                                .map(
                                                  (emp) => DataRow(cells: [
                                                    DataCell(
                                                      Text(emp.id.substring(
                                                          emp.id.length - 4)),
                                                    ),
                                                    DataCell(
                                                      Text(emp.name),
                                                    ),
                                                    DataCell(
                                                      Text(emp.type),
                                                    ),
                                                    DataCell(
                                                      Text(emp.expirationDate
                                                          .toString()),
                                                    ),
                                                    DataCell(
                                                      Text(emp.priceSupplier
                                                          .toString()),
                                                    ),
                                                    DataCell(
                                                      Text(emp.stock),
                                                    ),
                                                  ]),
                                                )
                                                .toList(),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                              height: 25,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 145),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Total stock:",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        stockTotal.toString(),
                                        style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 160),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Food stock:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        foodTotal.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 135),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Cleaning stock:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        cleaningTotal.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 115),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Technology stock:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        technologyTotal.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 135),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Clothing stock:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        clothingTotal.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          Container(
                              height: 20,
                              margin: const EdgeInsets.only(bottom: 10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 120),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Stationary stock:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        stationeryTotal.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 25,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 25),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Total price of the inventory:",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        precioTotal.toString(),
                                        style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 160),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Food price:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        foodPrice.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 135),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Cleaning price:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        cleaningPrice.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 115),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Technology price:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        technologyPrice.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          SizedBox(
                              height: 18,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 135),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Clothing price:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        clothingPrice.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          Container(
                              height: 20,
                              margin: const EdgeInsets.only(bottom: 10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 4, // 60%
                                    child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 120),
                                        alignment: Alignment.topCenter,
                                        color: const Color(0xFF7A93AC),
                                        child: const Text(
                                          "Stationary price:",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 31, 70, 109),
                                          ),
                                        )),
                                  ),
                                  Expanded(
                                    flex: 1, // 20%
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: const EdgeInsets.only(left: 3),
                                      color: const Color(0xFF7A93AC),
                                      child: Text(
                                        stationeryPrice.toString(),
                                        style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Color.fromARGB(255, 31, 70, 109),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          Container(
                            color: const Color(0xFF7A93AC),
                            child: Column(
                              children: [
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        color: const Color(0xFF7A93AC),
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.03,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        color: const Color(0xFF7A93AC),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: ElevatedButton.icon(
                                        icon: const ImageIcon(
                                          AssetImage(
                                            "images/calculate-logo.png",
                                          ),
                                          size: 27,
                                        ),
                                        label:
                                            const Text('Calculate Total Stock'),
                                        onPressed: () async {
                                          if (isConected) {
                                            final receivePort = ReceivePort();
                                            final prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            String? id = prefs.getString("id");
                                            await Isolate.spawn(
                                              _isolateService
                                                  .computeTotalStockProducts,
                                              [receivePort.sendPort, id],
                                            );
                                            receivePort.listen((total) {
                                              setState(() {
                                                stockTotal =
                                                    total[0].toString();
                                                foodTotal = total[1].toString();
                                                cleaningTotal =
                                                    total[2].toString();
                                                technologyTotal =
                                                    total[3].toString();
                                                clothingTotal =
                                                    total[4].toString();
                                                stationeryTotal =
                                                    total[5].toString();
                                              });
                                            });
                                          }
                                        },
                                        style: ElevatedButton.styleFrom(
                                          primary: const Color(0xFF243B4A),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        color: const Color(0xFF7A93AC),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        color: const Color(0xFF7A93AC),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: ElevatedButton.icon(
                                        icon: const ImageIcon(
                                          AssetImage(
                                            "images/calculate-logo.png",
                                          ),
                                          size: 27,
                                        ),
                                        label:
                                            const Text('Calculate Total Price'),
                                        onPressed: () async {
                                          if (isConected) {
                                            final receivePort = ReceivePort();
                                            final prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            String? id = prefs.getString("id");
                                            await Isolate.spawn(
                                              _isolateService
                                                  .computeTotalPriceProducts,
                                              [receivePort.sendPort, id],
                                            );
                                            receivePort.listen((total) {
                                              setState(() {
                                                precioTotal = "\$".toString() +
                                                    total[0].toString();
                                                foodPrice = "\$".toString() +
                                                    total[1].toString();
                                                cleaningPrice =
                                                    "\$".toString() +
                                                        total[2].toString();
                                                technologyPrice =
                                                    "\$".toString() +
                                                        total[3].toString();
                                                clothingPrice =
                                                    "\$".toString() +
                                                        total[4].toString();
                                                stationeryPrice =
                                                    "\$".toString() +
                                                        total[5].toString();
                                              });
                                            });
                                          }
                                        },
                                        style: ElevatedButton.styleFrom(
                                          primary: const Color(0xFF243B4A),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        color: const Color(0xFF7A93AC),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        color: const Color(0xFF7A93AC),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: ElevatedButton.icon(
                                        icon: const ImageIcon(
                                          AssetImage(
                                            "images/update.png",
                                          ),
                                          size: 27,
                                        ),
                                        label: const Text(
                                            'Update Product        '),
                                        onPressed: () async {
                                          print(products[0].name);
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      UpdateProductPage(
                                                        products: products,
                                                      )));
                                        },
                                        style: ElevatedButton.styleFrom(
                                          primary: const Color(0xFF243B4A),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        color: const Color(0xFF7A93AC),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ),
                          const SizedBox(height: 19)
                        ],
                      ),
                    ),
                  ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        currentIndex: index,
        onTap: (int index) {
          setState(() {
            this.index = index;
          });
          if (index == 0) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => HomePage()));
          }
          if (index == 1) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => (type == "suppliers")
                    ? const OrdersSuppliersPage()
                    : const OrderShopsPage()));
          }
          if (index == 2) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => DashboardPage()));
          }
          if (index == 3) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
        },
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/store.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/list.png"),
                color: Color(0xFF000000),
                size: 43,
              ),
              label: "Inventory"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/bar-graph.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Dashboard"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/gear.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Settings"),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
    ));
  }
}
