// ignore_for_file: use_key_in_widget_constructors
import 'dart:convert';
import 'dart:isolate';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/models/shop_product.dart';
import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';
import 'package:in_shop_flutter/src/pages/orders_suppliers.dart';
import 'package:in_shop_flutter/src/database/shop_product_database.dart';
import 'package:in_shop_flutter/src/services/isolate_service.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import 'package:path_provider/path_provider.dart';
import '../services/shared_prefs_service.dart';
import '../services/connectivity_service.dart';
import '../services/sqlite_service.dart';

class DashboardPage extends StatefulWidget {
  /*
   * String variable corresponding to the view 
   */
  static String id = 'DevelopmentPage';

  @override
  State<DashboardPage> createState() => _DashboardPagePageState();
}

class _DashboardPagePageState extends State<DashboardPage> {
  /*
   * Index variable for handling the navigation bar
   */
  int index = 0;

  /*
   * Arrays storing a supplier's products
   */
  List<ShopProduct> products = [];
  List<StockProducts> dataProduct = [];

  /*
   * Service locator for managing preferences and isolates.
   */
  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();
  final IsolateService _isolateService = GetIt.I.get<IsolateService>();

  final ConnectivityService _connectivityService =
      GetIt.I.get<ConnectivityService>();

  /*
   * Method to establish the type of user for which the information is to be obtained
   */
  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  // conectivity
  bool isConected = true;
  bool firstTime = true;

  late ShopProductDatabase database;

  Stream<bool> getConnection() async* {
    while (true) {
      await Future.delayed(const Duration(milliseconds: 500));
      yield await _connectivityService.checkInternetConnection();
    }
  }

  /*
   * Method to initialize the main status of dashboard view
   */
  @override
  void initState() {
    super.initState();
    userData();
    fetchProducts();
    getConnection().listen((value) {
      if (mounted) {
        if (!isConected && value) {
          setState(() {
            isConected = true;
          });
        } else if (!value && isConected) {
          setState(() {
            isConected = false;
          });
        }
      }
    });
  }

  /*
   * Obtain products from a specific supplier
   */
  void fetchProducts() async {
    // Obtain the port with which the connection to the isolate is established.
    final receivePort = ReceivePort();

    // Get user id from SharedPreferences
    String id = await _sharedPreferencesService.getStringValue("id");

    // Creation of the json file to store graph data
    String fileName = "GraphData" + id + ".json";
    var dir = await getTemporaryDirectory();
    File file = File(dir.path + "/" + fileName);

    if (file.existsSync()) {
      // Read cache file information
      final data = file.readAsStringSync();
      final res = json.decode(data);
      setState(() {
        dataProduct = res;
      });
    } else if (isConected) {
      // Obtain API user information
      await Isolate.spawn(
        _isolateService.fetchShopProducts,
        [receivePort.sendPort, id],
      );
      List<ShopProduct> temProds = await receivePort.first;
      products = temProds;
      setState(() {
        products = temProds;
      });
      List<StockProducts> dataProductTem = [];
      for (var i = 0; i < products.length; i++) {
        dataProductTem.add(StockProducts(
            products[i].name.toString().split(" ")[0],
            int.parse(products[i].stock)));
      }
      setState(() {
        dataProduct = dataProductTem;
      });
    }
  }

  /*
   * Obtain the current date and time at which products are sourced from a specific supplier
   */
  String dateToday = "Last update: " +
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day,
              DateTime.now().hour, DateTime.now().minute)
          .toString()
          .substring(0, 16)
          .replaceAll("-", "/");

  @override
  Widget build(BuildContext context) {
    // Data to be used for the construction of the bar chart
    final data = dataProduct;

    // Setting up the construction of the bar chart
    List<charts.Series<StockProducts, String>> series = [
      charts.Series<StockProducts, String>(
        id: "Barra 1",
        domainFn: (v, i) => v.id,
        measureFn: (v, i) => v.stock,
        colorFn: (_, __) => charts.Color.fromHex(code: "#7A93AC"),
        data: data,
        labelAccessorFn: (v, i) => v.stock.toString(),
      )
    ];

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text(
          "Dashboard",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => HomePage()));
            }),
        actions: const [
          CustomBuilder(),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      endDrawer: CustomDrawer(inventory: false),
      body: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          !isConected
              ? IntrinsicWidth(
                  child: Container(
                    height: 25,
                    width: MediaQuery.of(context).size.width,
                    color: const Color(0xFF7A93AC),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const [
                        Text("No internet connection"),
                      ],
                    ),
                  ),
                )
              : Container(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.all(25.0),
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width * 0.86,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  color: const Color(0xFF7A93AC),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      "Dashboard",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 7,
                    ),
                    Text(
                      dateToday,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          decoration: TextDecoration.underline),
                    )
                  ],
                ),
              ),
            ],
          ),
          const Text(
            "Products vs Stock",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          Container(
              margin: const EdgeInsets.only(left: 10.0, top: 15),
              color: Colors.transparent,
              child: Center(
                child: SizedBox(
                    height: 350,
                    child: charts.BarChart(
                      series,
                      barRendererDecorator: charts.BarLabelDecorator<String>(),
                      behaviors: [
                        charts.ChartTitle('Products',
                            behaviorPosition: charts.BehaviorPosition.bottom,
                            titleOutsideJustification:
                                charts.OutsideJustification.middleDrawArea),
                        charts.ChartTitle('Stock',
                            behaviorPosition: charts.BehaviorPosition.start,
                            titleOutsideJustification:
                                charts.OutsideJustification.middleDrawArea),
                      ],
                    )),
              ))
        ],
      )),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        currentIndex: index,
        onTap: (int index) {
          setState(() {
            this.index = index;
          });
          if (index == 0) {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomePage()));
          }
          if (index == 1) {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => (type == "suppliers")
                    ? const OrdersSuppliersPage()
                    : const OrderShopsPage()));
          }
          if (index == 2) {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => DashboardPage()));
          }
          if (index == 3) {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
        },
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/store.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/list.png"),
                color: Color(0xFF000000),
                size: 43,
              ),
              label: "Inventory"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/bar-graph.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Dashboard"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/gear.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Settings"),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
    ));
  }
}

/*
 * Class that creates the objects StockProducts
 */
class StockProducts {
  // Attributes
  final String id;
  final int stock;

  // Constructor method
  StockProducts(this.id, this.stock);
}
