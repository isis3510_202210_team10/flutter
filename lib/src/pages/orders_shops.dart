import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:get_it/get_it.dart';

import 'package:flutter/material.dart';
import 'package:in_shop_flutter/src/pages/dasboard.dart';
import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/map.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import 'package:ionicons/ionicons.dart';
import 'package:http/http.dart' as http;

import 'package:in_shop_flutter/src/models/orders.dart';
import 'package:in_shop_flutter/src/database/orders_database.dart';
import '../services/isolate_service.dart';
import '../services/shared_prefs_service.dart';
import '../services/sqlite_service.dart';
import '../services/connectivity_service.dart';
import '../pages/orders_suppliers.dart';

class OrderShopsPage extends StatefulWidget {
  static String id = 'orders';

  const OrderShopsPage({Key? key}) : super(key: key);

  @override
  State<OrderShopsPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrderShopsPage> {
  List<Orders> orders = [];
  final items = ['Profile', 'Log out'];
  final filter = [
    'ON_REVISION',
    'ACCEPTED',
    'DECLINED',
    'SHIPPING',
    'COMPLETED'
  ];
  String? selectedItem;
  GlobalKey navBarGlobalKey = GlobalKey(debugLabel: 'bottomAppBar');
  int index = 0;

  late String destination;

  late OrdersDatabase database;

  final IsolateService _isolateService = GetIt.I.get<IsolateService>();

  final ConnectivityService _connectivityService =
      GetIt.I.get<ConnectivityService>();

  final SqliteService _sqliteService = GetIt.I.get<SqliteService>();

  fetchOrders() async {
    final receivePort = ReceivePort();
    String id = await _sharedPreferencesService.getStringValue("id");
    await Isolate.spawn(
      _isolateService.fetchShopOrders,
      [receivePort.sendPort, id],
    );
    return await receivePort.first;
  }

  Future<List> getSupplier(id) async {
    print(id);
    String url = "https://inshop-uniandes.herokuapp.com/suppliers/" + id;
    try {
      var response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        return [jsonDecode(response.body)];
      } else {
        return Future.error("Server Error");
      }
    } catch (e) {
      return Future.error(e);
    }
  }

  bool isConected = true;
  bool firstTime = true;

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  Stream<bool> getConnection() async* {
    while (true) {
      await Future.delayed(const Duration(milliseconds: 500));
      yield await _connectivityService.checkInternetConnection();
    }
  }

  @override
  void initState() {
    super.initState();
    userData();
    getConnection().listen((value) {
      setState(() {
        if (!isConected && value || firstTime && value) {
          fetchState();
          isConected = true;
          firstTime = false;
          print("XDDDDD");
        } else if (!value) {
          isConected = false;
        } else {
          isConected = true;
        }
      });
    });
    $FloorOrdersDatabase
        .databaseBuilder('orders_database.db')
        .build()
        .then((value) async {
      database = value;
      // get orders from local db
      List<Orders> prueba = await _sqliteService.retrieveOrders(database);
      setState(() {
        orders = prueba;
      });
    });
  }

  fetchState() async {
    List<Orders> tmpOrders = await fetchOrders();
    if (mounted) {
      setState(() {
        orders = tmpOrders;
      });
      await _sqliteService.deleteOrders(database); // clean local db
      await _sqliteService.addOrder(database, orders);
    }
  }

  /*
   * Dispose for connectivity management
   */
  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Orders",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: const [
          CustomBuilder(),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        currentIndex: index,
        onTap: (int index) {
          setState(() {
            this.index = index;
          });
          if (index == 0) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => HomePage()));
          }
          if (index == 1) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => (type == "suppliers")
                    ? const OrdersSuppliersPage()
                    : const OrderShopsPage()));
          }
          if (index == 2) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => DashboardPage()));
          }
          if (index == 3) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
        },
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/store.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/list.png"),
                color: Color(0xFF000000),
                size: 43,
              ),
              label: "Inventory"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/bar-graph.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Dashboard"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/gear.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Settings"),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      endDrawer: CustomDrawer(inventory: true),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            !isConected
                ? IntrinsicWidth(
                    child: Container(
                      height: 25,
                      width: MediaQuery.of(context).size.width,
                      color: const Color(0xFF7A93AC),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: const [
                          Text("No internet connection"),
                        ],
                      ),
                    ),
                  )
                : Container(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.all(25.0),
                  height: MediaQuery.of(context).size.height * 0.1,
                  width: MediaQuery.of(context).size.width * 0.83,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: const Color(0xFF7A93AC),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        "Orders",
                        style: TextStyle(
                            fontSize: 23, fontWeight: FontWeight.bold),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text(
                            "Filter:",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(width: 15),
                          Container(
                              height:
                                  MediaQuery.of(context).size.height * 0.026,
                              width: MediaQuery.of(context).size.width * 0.61,
                              decoration: const BoxDecoration(
                                color: Color(0xFFCFDBD5),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.0),
                                ),
                              ),
                              child: DropdownButton(
                                  isExpanded: true,
                                  items: filter.map((String s) {
                                    return DropdownMenuItem(
                                        value: s, child: Text(s));
                                  }).toList(),
                                  onChanged: (String? value) => setState(() {
                                        selectedItem = value ?? "";
                                      }),
                                  hint: Center(
                                      child: Text(
                                          selectedItem ?? "Select Filter"))))
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 25),
                    height: MediaQuery.of(context).size.height * 0.465,
                    width: MediaQuery.of(context).size.width * 0.83,
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          if (orders.isNotEmpty)
                            ListView.builder(
                                addAutomaticKeepAlives: false,
                                addRepaintBoundaries: false,                              
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: orders.length,
                                itemBuilder: (context, i) {
                                  if (selectedItem == orders[i].status ||
                                      selectedItem == null) {
                                    return Container(
                                      padding: const EdgeInsets.only(
                                          top: 15, left: 15, right: 15),
                                      margin: const EdgeInsets.all(7.0),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.185,
                                      width: MediaQuery.of(context).size.width *
                                          0.83,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                        color: const Color(0xFFD5A021),
                                      ),
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              const Text(
                                                "Date: ",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Text(orders[i].initialDate),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.16,
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              const Text(
                                                "PO#: ",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Text(orders[i].id),
                                            ],
                                          ),
                                          Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.0066,
                                          ),
                                          Row(
                                            children: [
                                              Image.asset(
                                                  "images/orderList.png"),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.01,
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(children: [
                                                    const Text(
                                                      "Supplier: ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  Text(orders[i].supplierId)
                                                  ]),
                                                  Row(children: [
                                                    const Text(
                                                      "Delivery date: ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(orders[i].arrivalDate)
                                                  ]),
                                                  Row(children: [
                                                    const Text(
                                                      "Total: ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text("\$" +
                                                        orders[i]
                                                            .total
                                                            .toString())
                                                  ]),
                                                ],
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                transform:
                                                    Matrix4.translationValues(
                                                        0.0, -1.0, 0.0),
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.04,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.15,
                                                child: TextButton(
                                                  child: const Text(
                                                    "Map",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 8),
                                                  ),
                                                  onPressed: () {
                                                    destination =
                                                        orders[i].address;
                                                    _sendDataToSecondScreen(
                                                        context);
                                                  },
                                                ),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                  color:
                                                      const Color(0xFF243B4A),
                                                ),
                                              ),
                                              Expanded(
                                                  flex: 5,
                                                  child: Container(
                                                    color:
                                                        const Color(0xFFD5A021),
                                                  )),
                                              Container(
                                                transform:
                                                    Matrix4.translationValues(
                                                        0.0, -5.0, 0.0),
                                                margin: const EdgeInsets.only(
                                                    left: 5),
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.05,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.11,
                                                child: TextButton(
                                                  child: const ImageIcon(
                                                    AssetImage(
                                                        "images/can.png"),
                                                    color: Colors.white,
                                                  ),
                                                  onPressed: () {},
                                                ),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                  color:
                                                      const Color(0xFF243B4A),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  } else {
                                    return const SizedBox();
                                  }
                                })
                          else if (!isConected)
                            Column(
                              children: [
                                Image.asset(
                                  'images/wifi-off.png',
                                ),
                                const SizedBox(
                                  height: 15.0,
                                ),
                                const Text(
                                  'No internet connection',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: Color(0xFFCFDBD5)),
                                ),
                                const SizedBox(
                                  height: 6.0,
                                ),
                                const Text(
                                  'Check your internet connection status.',
                                  style: TextStyle(
                                      fontSize: 15, color: Color(0xFFCFDBD5)),
                                ),
                                const SizedBox(
                                  height: 10.0,
                                ),
                              ],
                            )
                          else
                            Column(
                                      children: const [
                                        Center(
                                          child: Icon(Icons.search),
                                        ),
                                        Text("Nothing found in search"),
                                      ],
                            )
                        ],
                      ),
                    ),
                  ),
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 25),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: const Color(0xFF243B4A),
                    ),
                    height: MediaQuery.of(context).size.height * 0.066,
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: ElevatedButton.icon(
                      icon: const Icon(Ionicons.add,
                          color: Colors.white, size: 25),
                      label: const Text(
                        'Add orders',
                        style: TextStyle(fontSize: 17, color: Colors.white),
                      ),
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        primary: const Color(0xFF243B4A),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                    ),
                  ),
                ]),
          ],
        ),
      ),
    );
  }

  void _sendDataToSecondScreen(BuildContext context) {
    String textToSend = destination;
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MapPage(
            destination: textToSend,
          ),
        ));
  }
}
