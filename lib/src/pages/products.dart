import 'dart:isolate';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/home.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';
import 'package:in_shop_flutter/src/pages/suppliers.dart';
import 'package:in_shop_flutter/src/widgets/custom_builder.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import '../database/suppiler_product_database.dart';
import '../models/supplier_product.dart';
import '../services/isolate_service.dart';
import '../services/shared_prefs_service.dart';
import '../services/sqlite_service.dart';
import 'orders_suppliers.dart';

class ProductsPage extends StatefulWidget {
  /*
   * String variable corresponding to the view 
   */
  static String id = 'products';

  /*
   * Widget keys InventorySupplierPage
   */
  const ProductsPage({Key? key}) : super(key: key);

  @override
  State<ProductsPage> createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  /*
   * Array storing a supplier's products
   */
  List<SupplierProduct> products = [];

  /*
   * Index variable for handling the navigation bar
   */
  int index = 0;

  /*
   * Variable that manages the state of charge 
   */
  bool isLoading = false;

  /*
   * Arrangement of drawer items
   */
  final items = ['Profile', 'Log out'];

  /*
   * Variable to establish the connection to the database
   */
  late SupplierProductDatabase database;

  /*
   * Service locator for managing preferences, isolates and the local database.
   */
  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();
  final IsolateService _isolateService = GetIt.I.get<IsolateService>();
  final SqliteService _sqliteService = GetIt.I.get<SqliteService>();

  /*
   * Method to establish the type of user for which the information is to be obtained
   */
  String type = "suppliers";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    setState(() {
      type = typeUser;
    });
  }

  /*
   * Method to initialize the main status of a supplier's product view
   */
  @override
  void initState() {
    super.initState();
    userData();
    $FloorSupplierProductDatabase
        .databaseBuilder('suppiler_product_database.db')
        .build()
        .then((value) async {
      database = value;
      // Get products from local database
      List<SupplierProduct> prueba =
          await _sqliteService.retrieveSupplierProducts(database);
      setState(() {
        products = prueba;
      });
      // Get producs from api to update changes in local database
      List<SupplierProduct> tpmProducts = await fetchProducts();
      if (mounted) {
        setState(() {
          products = tpmProducts;
        });
        await _sqliteService
            .deleteSupplierProducts(database); // Clean local database
        await _sqliteService.addSupplierProducts(
            database, products); // Save in local database
      }
    });
  }

  /*
   * Obtain products from a specific supplier
   */
  fetchProducts() async {
    final receivePort = ReceivePort();
    String id = await _sharedPreferencesService.getStringValue("idSupplier");
    await Isolate.spawn(
      _isolateService.fetchSupplierProducts,
      [receivePort.sendPort, id],
    );
    return await receivePort.first;
  }

  /*
   * Obtain the current date and time at which products are sourced from a specific supplier
   */
  String dateToday = "Last update: " +
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day,
              DateTime.now().hour, DateTime.now().minute)
          .toString()
          .substring(0, 16)
          .replaceAll("-", "/");

  /*
   * Modify the string corresponding to the unit price product
   */
  String unitPriceProduct(unitPrice) {
    String unitPriceString = "\$" + unitPrice.toString() + " COP";
    return unitPriceString;
  }

  /*
   * Modify the string corresponding to the id product supplier 
   */
  String idProductSupplier(idProduct) {
    int lengthAdress = idProduct.length;
    if (lengthAdress > 20) {
      lengthAdress = 24;
    }
    return idProduct.toString().substring(0, lengthAdress);
  }

  /*
   * Modify the string corresponding to the name supplier
   */
  String nameProductSupplier(nameProduct) {
    int lengthAdress = nameProduct.length;
    if (lengthAdress > 20) {
      lengthAdress = 17;
    }
    return nameProduct.toString().substring(0, lengthAdress);
  }

  /*
   * Modify the string corresponding to the sdescription product
   */
  String descriptionProductSupplier(descriptionProduct) {
    int lengthAdress = descriptionProduct.length;
    if (lengthAdress > 14) {
      lengthAdress = 12;
    }
    return descriptionProduct.toString().substring(0, lengthAdress);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Products",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.03,
          ),
        ),
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const SuppliersPage()));
            }),
        actions: const [
          CustomBuilder(),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        currentIndex: index,
        onTap: (int index) {
          setState(() {
            this.index = index;
          });
          if (index == 0) {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => HomePage()));
          }
          if (index == 1) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => (type == "suppliers")
                    ? const OrdersSuppliersPage()
                    : const OrderShopsPage()));
          }
          if (index == 2) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
          if (index == 3) {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DevelopmentPage()));
          }
        },
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/store.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/list.png"),
                color: Color(0xFF000000),
                size: 43,
              ),
              label: "Inventory"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/bar-graph.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Dashboard"),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/gear.png"),
                color: Color(0xFF000000),
                size: 35,
              ),
              label: "Settings"),
        ],
        backgroundColor: const Color(0xFFD5A021),
      ),
      endDrawer: CustomDrawer(inventory: true),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.all(25.0),
                  height: MediaQuery.of(context).size.height * 0.1,
                  width: MediaQuery.of(context).size.width * 0.86,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: const Color(0xFF7A93AC),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        "Products",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 7,
                      ),
                      Text(
                        dateToday,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            decoration: TextDecoration.underline),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 25),
                    height: MediaQuery.of(context).size.height * 0.5,
                    width: MediaQuery.of(context).size.width * 0.86,
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          if (products.isNotEmpty)
                            ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: products.length,
                                itemBuilder: (context, i) {
                                  return Container(
                                    padding: const EdgeInsets.only(
                                        top: 12, left: 15, right: 15),
                                    margin: const EdgeInsets.all(7.0),
                                    height: MediaQuery.of(context).size.height *
                                        0.185,
                                    width: MediaQuery.of(context).size.width *
                                        0.83,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: const Color(0xFFD5A021),
                                    ),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            const Text(
                                              "ProductId: ",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(idProductSupplier(
                                                products[i].id)),
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.16,
                                            ),
                                          ],
                                        ),
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.012,
                                        ),
                                        Row(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(8.0),
                                              child: CachedNetworkImage(
                                                imageUrl: products[i]
                                                    .image
                                                    .replaceAll("300", "80"),
                                                placeholder: (context, url) =>
                                                    const ColoredBox(
                                                  color: Color(0xFFD5A021),
                                                  child: Center(
                                                      child:
                                                          CircularProgressIndicator()),
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        const ColoredBox(
                                                  color: Color(0xFFD5A021),
                                                  child: Icon(Icons.error,
                                                      size: 50,
                                                      color: Colors.black),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                            ),
                                            Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Wrap(children: [
                                                  const Text(
                                                    "Name: ",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(nameProductSupplier(
                                                      products[i].name))
                                                ]),
                                                Row(children: [
                                                  const Text(
                                                    "Description: ",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(products[i].description)
                                                ]),
                                                Wrap(
                                                    direction: Axis.horizontal,
                                                    children: <Widget>[
                                                      const Text(
                                                        "Unit Price: ",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(unitPriceProduct(
                                                          products[i]
                                                              .priceSupplier
                                                              .toString()))
                                                    ]),
                                                Wrap(
                                                    direction: Axis.horizontal,
                                                    children: <Widget>[
                                                      const Text(
                                                        "Stock: ",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(products[i]
                                                              .stock
                                                              .toString() +
                                                          " Units")
                                                    ]),
                                              ],
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                }),
                        ],
                      ),
                    ),
                  ),
                ]),
          ],
        ),
      ),
    );
  }
}
