import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/pages/dasboard.dart';
import 'package:in_shop_flutter/src/pages/development_page.dart';
import 'package:in_shop_flutter/src/pages/inventory_shop.dart';
import 'package:in_shop_flutter/src/pages/orders_shops.dart';
import 'package:in_shop_flutter/src/models/shop_product.dart';
import 'package:in_shop_flutter/src/database/shop_product_database.dart';
import 'package:in_shop_flutter/src/pages/suppliers.dart';
import 'package:in_shop_flutter/src/services/utils_service.dart';
import 'package:in_shop_flutter/src/widgets/custom_drawer.dart';
import '../services/isolate_service.dart';
import '../services/shared_prefs_service.dart';
import '../services/sqlite_service.dart';
import 'inventory_supplier.dart';
import 'orders_suppliers.dart';

class HomePage extends StatefulWidget {
  static String id = 'home';

  // ignore: prefer_typing_uninitialized_variables
  var userType;
  HomePage({Key? key, this.userType}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final items = ['Profile', 'Log out'];
  GlobalKey navBarGlobalKey = GlobalKey(debugLabel: 'bottomAppBar');
  int index = 0;

  List<ShopProduct> products = [];
  String productToExpire = "";
  String expDate = "";
  bool isLoading = false;

  File? _image;

  late ShopProductDatabase database;

  final MySharedPreferences _sharedPreferencesService =
      GetIt.I.get<MySharedPreferences>();

  final IsolateService _isolateService = GetIt.I.get<IsolateService>();

  final UtilsService _utilsService = GetIt.I.get<UtilsService>();

  final SqliteService _sqliteService = GetIt.I.get<SqliteService>();

  String type = "shops";
  userData() async {
    String typeUser =
        await _sharedPreferencesService.getStringValue('typeUser');
    print(typeUser);
    setState(() {
      type = typeUser;
    });
  }

  inicializar() async {
    await userData();
    await loadPhoto();
    $FloorShopProductDatabase
        .databaseBuilder('shop_product_database.db')
        .build()
        .then((value) async {
      database = value;
      // get products from local db
      List<ShopProduct> prueba =
          await _sqliteService.retrieveShopProducts(database);
      setState(() {
        products = prueba;
      });
      if (mounted) {
        prueba = _utilsService.obtenerProductosPorExpirar(products);
        setState(() {
          productToExpire = _utilsService.respuestaExpiracion(prueba);
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    inicializar();
  }

  loadPhoto() async {
    try {
      String? profilePath =
          await _sharedPreferencesService.getStringValue("profile_image");

      if (profilePath != "") {
        setState(() {
          _image = File(profilePath);
        });
      }
    } catch (e) {
      //pass
    }
  }

  final String logoSupplierUrl =
      "https://cdn.discordapp.com/attachments/807264106474111017/968726466860838922/hotel-supplier.png";
  final String logoInventoryUrl =
      "https://cdn.discordapp.com/attachments/807264106474111017/968733103952646144/shipping.png";
  final String logoOrderUrl =
      "https://cdn.discordapp.com/attachments/807264106474111017/968733161582366750/checkout.png";
  final String logoAlertUrl =
      "https://cdn.discordapp.com/attachments/807264106474111017/968733438666493962/warning.png";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [
            Builder(
              builder: (context) => Container(
                padding:
                    const EdgeInsets.only(top: 5, left: 5, bottom: 5, right: 9),
                child: InkWell(
                  onTap: () => Scaffold.of(context)
                      .openEndDrawer(), // Handle your callback.
                  splashColor: Colors.brown.withOpacity(0.5),
                  child: CircleAvatar(
                    radius:
                        MediaQuery.of(context).size.width / 16, // Image radius
                    backgroundImage: _image == null
                        ? const AssetImage("images/user.png") as ImageProvider
                        : FileImage(_image!),
                  ),
                ),
              ),
            )
          ],
          backgroundColor: const Color(0xFFD5A021),
        ),
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          currentIndex: index,
          onTap: (int index) {
            setState(() {
              this.index = index;
            });
            if (index == 0) {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => HomePage()));
            }
            if (index == 1) {
              print(type);
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => (type == "suppliers")
                      ? const OrdersSuppliersPage()
                      : const OrderShopsPage()));
            }
            if (index == 2) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => DashboardPage()));
            }
            if (index == 3) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => DevelopmentPage()));
            }
          },
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/store.png"),
                  color: Color(0xFF000000),
                  size: 35,
                ),
                label: "Home"),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/list.png"),
                  color: Color(0xFF000000),
                  size: 43,
                ),
                label: "Inventory"),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/bar-graph.png"),
                  color: Color(0xFF000000),
                  size: 35,
                ),
                label: "Dashboard"),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("images/gear.png"),
                  color: Color(0xFF000000),
                  size: 35,
                ),
                label: "Settings"),
          ],
          backgroundColor: const Color(0xFFD5A021),
        ),
        endDrawer: CustomDrawer(inventory: true),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.all(5),
                    margin: const EdgeInsets.all(25.0),
                    height: MediaQuery.of(context).size.height * 0.13,
                    width: MediaQuery.of(context).size.width * 0.27,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: const Color(0xFFD5A021),
                    ),
                    child: TextButton(
                        child: Image.asset("images/hotel-supplier.png"),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => const SuppliersPage()));
                        }),
                  ),
                  Container(
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: const Color(0xFF7A93AC),
                    ),
                    margin: const EdgeInsets.all(25.0),
                    height: MediaQuery.of(context).size.height * 0.13,
                    width: MediaQuery.of(context).size.width * 0.44,
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const SuppliersPage()));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text(
                            "Suppliers",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                          Container(height: 3),
                          const Text("Food product",
                              style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black)),
                          const Text("suppliers",
                              style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black)),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Row(
                  //ROW 2
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: const Color(0xFF7A93AC),
                        ),
                        margin: const EdgeInsets.all(25.0),
                        height: MediaQuery.of(context).size.height * 0.13,
                        width: MediaQuery.of(context).size.width * 0.44,
                        child: TextButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => (type == "suppliers")
                                    ? InventorySupplierPage()
                                    : const InventoryShopPage()));
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const Text(
                                "Inventory",
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              Container(height: 3),
                              const Text("Bakery inventory",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.black)),
                              const Text("tracking",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.black)),
                            ],
                          ),
                        )),
                    Container(
                      padding: const EdgeInsets.all(5),
                      margin: const EdgeInsets.all(25.0),
                      height: MediaQuery.of(context).size.height * 0.13,
                      width: MediaQuery.of(context).size.width * 0.27,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: const Color(0xFFD5A021),
                      ),
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => InventoryShopPage()));
                        },
                        child: Image.asset('images/shipping.png'),
                      ),
                    ),
                  ]),
              Row(
                  // ROW 3
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: const Color(0xFFD5A021),
                        ),
                        margin: const EdgeInsets.all(25.0),
                        height: MediaQuery.of(context).size.height * 0.13,
                        width: MediaQuery.of(context).size.width * 0.27,
                        child: TextButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => (type == "suppliers")
                                    ? const OrdersSuppliersPage()
                                    : const OrderShopsPage()));
                          },
                          child: Image.asset('images/checkout.png'),
                        )),
                    Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: const Color(0xFF7A93AC),
                      ),
                      margin: const EdgeInsets.all(25.0),
                      height: MediaQuery.of(context).size.height * 0.13,
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => (type == "suppliers")
                                  ? const OrdersSuppliersPage()
                                  : const OrderShopsPage()));
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              "Orders",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                            Container(height: 3),
                            const Text("Product",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.black)),
                            const Text("purchase orders",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.black)),
                          ],
                        ),
                      ),
                    )
                  ]),
              Row(
                  // ROW 4
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: const Color(0xFF7A93AC),
                        ),
                        margin: const EdgeInsets.all(25.0),
                        height: MediaQuery.of(context).size.height * 0.13,
                        width: MediaQuery.of(context).size.width * 0.44,
                        child: TextButton(
                          onPressed: () {
                            showAlertDialog(BuildContext context) {
                              // set up the button
                              Widget okButton = TextButton(
                                child: const Text("OK"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              );

                              // set up the AlertDialog
                              AlertDialog alert = AlertDialog(
                                title: const Text("Product expiration alert"),
                                content: Text(productToExpire),
                                actions: [
                                  okButton,
                                ],
                              );

                              // show the dialog
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return alert;
                                },
                              );
                            }

                            showAlertDialog(context);
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const Text(
                                "Alerts",
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              Container(height: 3),
                              const Text("Out-of-stock",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.black)),
                              const Text("alerts",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.black)),
                            ],
                          ),
                        )),
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: const Color(0xFFD5A021),
                        ),
                        margin: const EdgeInsets.all(25.0),
                        height: MediaQuery.of(context).size.height * 0.13,
                        width: MediaQuery.of(context).size.width * 0.27,
                        child: TextButton(
                          onPressed: () {
                            showAlertDialog(BuildContext context) {
                              // set up the button
                              Widget okButton = TextButton(
                                child: const Text("OK"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              );

                              // set up the AlertDialog
                              AlertDialog alert = AlertDialog(
                                title: const Text("Product expiration alert"),
                                content: Text(productToExpire),
                                actions: [
                                  okButton,
                                ],
                              );

                              // show the dialog
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return alert;
                                },
                              );
                            }

                            showAlertDialog(context);
                          },
                          child: Image.asset('images/warning.png'),
                        )),
                  ]),
            ],
          ),
        ));
  }
}
