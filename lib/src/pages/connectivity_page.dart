import 'package:flutter/material.dart';

class ConnectivityPage extends StatefulWidget {
  static String id = 'ConnectivityPage';

  var connectivity;
  ConnectivityPage({Key? key, this.connectivity}) : super(key: key);

  @override
  State<ConnectivityPage> createState() => _ConnectivityPageState();
}

class _ConnectivityPageState extends State<ConnectivityPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xFF7A93AC),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 25, 0),
              child: Align(
                alignment: Alignment.center,
                child: buildBanner(),
              )),
          Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 25, 0),
              child: Align(
                alignment: Alignment.center,
                child: buildButtonTryAgain(),
              ))
        ],
      ),
    );
  }

  Widget buildBanner() => Column(
        children: [
          Image.asset(
            'images/wifi-off.png',
          ),
          const SizedBox(
            height: 15.0,
          ),
          const Text(
            'No internet connection',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Color(0xFFCFDBD5)),
          ),
          const SizedBox(
            height: 6.0,
          ),
          const Text(
            'Check your internet connection status.',
            style: TextStyle(fontSize: 15, color: Color(0xFFCFDBD5)),
          ),
          const SizedBox(
            height: 10.0,
          ),
        ],
      );

  Widget buildButtonTryAgain() => Column(
        children: [
          SizedBox(
            width: 160.0,
            child: ElevatedButton(
              onPressed: () {
                setState(() {
                  widget.connectivity;
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: const [
                  Text(
                    'Please try again',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                        color: Colors.black87),
                  )
                ],
              ),
              style: ElevatedButton.styleFrom(
                primary: const Color(0xFFD5A021),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
            ),
          ),
        ],
      );
}
