// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orders_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorOrdersDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$OrdersDatabaseBuilder databaseBuilder(String name) =>
      _$OrdersDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$OrdersDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$OrdersDatabaseBuilder(null);
}

class _$OrdersDatabaseBuilder {
  _$OrdersDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$OrdersDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$OrdersDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<OrdersDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$OrdersDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$OrdersDatabase extends OrdersDatabase {
  _$OrdersDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  OrdersDAO? _ordersDAOInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Orders` (`id` TEXT NOT NULL, `initialDate` TEXT NOT NULL, `arrivalDate` TEXT NOT NULL, `address` TEXT NOT NULL, `status` TEXT NOT NULL, `total` TEXT NOT NULL, `shopId` TEXT NOT NULL, `supplierId` TEXT NOT NULL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  OrdersDAO get ordersDAO {
    return _ordersDAOInstance ??= _$OrdersDAO(database, changeListener);
  }
}

class _$OrdersDAO extends OrdersDAO {
  _$OrdersDAO(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _ordersInsertionAdapter = InsertionAdapter(
            database,
            'Orders',
            (Orders item) => <String, Object?>{
                  'id': item.id,
                  'initialDate': item.initialDate,
                  'arrivalDate': item.arrivalDate,
                  'address': item.address,
                  'status': item.status,
                  'total': item.total,
                  'shopId': item.shopId,
                  'supplierId': item.supplierId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Orders> _ordersInsertionAdapter;

  @override
  Future<List<Orders>> retrieveOrders() async {
    return _queryAdapter.queryList('SELECT * FROM Orders',
        mapper: (Map<String, Object?> row) => Orders(
            id: row['id'] as String,
            initialDate: row['initialDate'] as String,
            arrivalDate: row['arrivalDate'] as String,
            address: row['address'] as String,
            status: row['status'] as String,
            total: row['total'] as String,
            shopId: row['shopId'] as String,
            supplierId: row['supplierId'] as String));
  }

  @override
  Future<Orders?> deleteOrder(int id) async {
    return _queryAdapter.query('DELETE FROM Orders WHERE id = ?1',
        mapper: (Map<String, Object?> row) => Orders(
            id: row['id'] as String,
            initialDate: row['initialDate'] as String,
            arrivalDate: row['arrivalDate'] as String,
            address: row['address'] as String,
            status: row['status'] as String,
            total: row['total'] as String,
            shopId: row['shopId'] as String,
            supplierId: row['supplierId'] as String),
        arguments: [id]);
  }

  @override
  Future<List<Orders>> deleteOrders() async {
    return _queryAdapter.queryList('DELETE FROM Orders',
        mapper: (Map<String, Object?> row) => Orders(
            id: row['id'] as String,
            initialDate: row['initialDate'] as String,
            arrivalDate: row['arrivalDate'] as String,
            address: row['address'] as String,
            status: row['status'] as String,
            total: row['total'] as String,
            shopId: row['shopId'] as String,
            supplierId: row['supplierId'] as String));
  }

  @override
  Future<List<int>> inserOrder(List<Orders> orders) {
    return _ordersInsertionAdapter.insertListAndReturnIds(
        orders, OnConflictStrategy.abort);
  }
}
