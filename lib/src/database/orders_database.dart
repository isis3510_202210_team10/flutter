import 'dart:async';
import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/orders.dart';
import 'package:in_shop_flutter/src/models/orders_dao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;


part 'orders_database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [Orders])
abstract class OrdersDatabase extends FloorDatabase {
  OrdersDAO get ordersDAO;
}