import "dart:async";
import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/supplier_product.dart';
import 'package:in_shop_flutter/src/models/supplier_product_dao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'suppiler_product_database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [SupplierProduct])
abstract class SupplierProductDatabase extends FloorDatabase {
  SupplierProductDAO get supplierProductDAO;
}
