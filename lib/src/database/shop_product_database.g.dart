// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop_product_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorShopProductDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$ShopProductDatabaseBuilder databaseBuilder(String name) =>
      _$ShopProductDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$ShopProductDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$ShopProductDatabaseBuilder(null);
}

class _$ShopProductDatabaseBuilder {
  _$ShopProductDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$ShopProductDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$ShopProductDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<ShopProductDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$ShopProductDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$ShopProductDatabase extends ShopProductDatabase {
  _$ShopProductDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  ShopProductDAO? _shopProductDAOInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `ShopProduct` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `description` TEXT NOT NULL, `type` TEXT NOT NULL, `image` TEXT NOT NULL, `expirationDate` TEXT NOT NULL, `priceSupplier` TEXT NOT NULL, `stock` TEXT NOT NULL, `shopId` TEXT NOT NULL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  ShopProductDAO get shopProductDAO {
    return _shopProductDAOInstance ??=
        _$ShopProductDAO(database, changeListener);
  }
}

class _$ShopProductDAO extends ShopProductDAO {
  _$ShopProductDAO(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _shopProductInsertionAdapter = InsertionAdapter(
            database,
            'ShopProduct',
            (ShopProduct item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'description': item.description,
                  'type': item.type,
                  'image': item.image,
                  'expirationDate': item.expirationDate,
                  'priceSupplier': item.priceSupplier,
                  'stock': item.stock,
                  'shopId': item.shopId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<ShopProduct> _shopProductInsertionAdapter;

  @override
  Future<List<ShopProduct>> retrieveProducts() async {
    return _queryAdapter.queryList('SELECT * FROM ShopProduct',
        mapper: (Map<String, Object?> row) => ShopProduct(
            id: row['id'] as String,
            name: row['name'] as String,
            description: row['description'] as String,
            type: row['type'] as String,
            image: row['image'] as String,
            expirationDate: row['expirationDate'] as String,
            priceSupplier: row['priceSupplier'] as String,
            stock: row['stock'] as String,
            shopId: row['shopId'] as String));
  }

  @override
  Future<ShopProduct?> deleteProduct(int id) async {
    return _queryAdapter.query('DELETE FROM ShopProduct WHERE id = ?1',
        mapper: (Map<String, Object?> row) => ShopProduct(
            id: row['id'] as String,
            name: row['name'] as String,
            description: row['description'] as String,
            type: row['type'] as String,
            image: row['image'] as String,
            expirationDate: row['expirationDate'] as String,
            priceSupplier: row['priceSupplier'] as String,
            stock: row['stock'] as String,
            shopId: row['shopId'] as String),
        arguments: [id]);
  }

  @override
  Future<List<ShopProduct>> deleteProducts() async {
    return _queryAdapter.queryList('DELETE FROM ShopProduct',
        mapper: (Map<String, Object?> row) => ShopProduct(
            id: row['id'] as String,
            name: row['name'] as String,
            description: row['description'] as String,
            type: row['type'] as String,
            image: row['image'] as String,
            expirationDate: row['expirationDate'] as String,
            priceSupplier: row['priceSupplier'] as String,
            stock: row['stock'] as String,
            shopId: row['shopId'] as String));
  }

  @override
  Future<List<int>> inserProduct(List<ShopProduct> product) {
    return _shopProductInsertionAdapter.insertListAndReturnIds(
        product, OnConflictStrategy.abort);
  }
}
