import 'dart:async';
import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/shop_product.dart';
import 'package:in_shop_flutter/src/models/shop_product_dao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'shop_product_database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [ShopProduct])
abstract class ShopProductDatabase extends FloorDatabase {
  ShopProductDAO get shopProductDAO;
}
