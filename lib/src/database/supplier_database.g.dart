// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'supplier_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorSupplierDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$SupplierDatabaseBuilder databaseBuilder(String name) =>
      _$SupplierDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$SupplierDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$SupplierDatabaseBuilder(null);
}

class _$SupplierDatabaseBuilder {
  _$SupplierDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$SupplierDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$SupplierDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<SupplierDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$SupplierDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$SupplierDatabase extends SupplierDatabase {
  _$SupplierDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  SupplierDAO? _supplierDAOInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Supplier` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `email` TEXT NOT NULL, `address` TEXT NOT NULL, `phone` TEXT NOT NULL, `companyName` TEXT NOT NULL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  SupplierDAO get supplierDAO {
    return _supplierDAOInstance ??= _$SupplierDAO(database, changeListener);
  }
}

class _$SupplierDAO extends SupplierDAO {
  _$SupplierDAO(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _supplierInsertionAdapter = InsertionAdapter(
            database,
            'Supplier',
            (Supplier item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'email': item.email,
                  'address': item.address,
                  'phone': item.phone,
                  'companyName': item.companyName
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Supplier> _supplierInsertionAdapter;

  @override
  Future<List<Supplier>> retrieveSuppliers() async {
    return _queryAdapter.queryList('SELECT * FROM Supplier',
        mapper: (Map<String, Object?> row) => Supplier(
            id: row['id'] as String,
            name: row['name'] as String,
            email: row['email'] as String,
            address: row['address'] as String,
            phone: row['phone'] as String,
            companyName: row['companyName'] as String));
  }

  @override
  Future<Supplier?> deleteSupplier(int id) async {
    return _queryAdapter.query('DELETE FROM Supplier WHERE id = ?1',
        mapper: (Map<String, Object?> row) => Supplier(
            id: row['id'] as String,
            name: row['name'] as String,
            email: row['email'] as String,
            address: row['address'] as String,
            phone: row['phone'] as String,
            companyName: row['companyName'] as String),
        arguments: [id]);
  }

  @override
  Future<List<Supplier>> deleteSuppliers() async {
    return _queryAdapter.queryList('DELETE FROM Supplier',
        mapper: (Map<String, Object?> row) => Supplier(
            id: row['id'] as String,
            name: row['name'] as String,
            email: row['email'] as String,
            address: row['address'] as String,
            phone: row['phone'] as String,
            companyName: row['companyName'] as String));
  }

  @override
  Future<List<int>> insertSupplier(List<Supplier> supplier) {
    return _supplierInsertionAdapter.insertListAndReturnIds(
        supplier, OnConflictStrategy.abort);
  }
}
