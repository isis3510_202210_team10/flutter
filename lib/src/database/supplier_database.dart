import 'dart:async';
import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/supplier.dart';
import 'package:in_shop_flutter/src/models/supplier_dao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'supplier_database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [Supplier])
abstract class SupplierDatabase extends FloorDatabase {
  SupplierDAO get supplierDAO;
}
