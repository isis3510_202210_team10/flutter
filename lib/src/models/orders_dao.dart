import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/orders.dart';

@dao
abstract class OrdersDAO {
  @insert
  Future<List<int>> inserOrder(List<Orders> orders);

  @Query('SELECT * FROM Orders')
  Future<List<Orders>> retrieveOrders();

  @Query('DELETE FROM Orders WHERE id = :id')
  Future<Orders?> deleteOrder(int id);

  @Query('DELETE FROM Orders')
  Future<List<Orders>> deleteOrders();
}