import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/supplier.dart';

@dao
abstract class SupplierDAO {
  @insert
  Future<List<int>> insertSupplier(List<Supplier> supplier);

  @Query('SELECT * FROM Supplier')
  Future<List<Supplier>> retrieveSuppliers();

  @Query('DELETE FROM Supplier WHERE id = :id')
  Future<Supplier?> deleteSupplier(int id);

  @Query('DELETE FROM Supplier')
  Future<List<Supplier>> deleteSuppliers();
}
