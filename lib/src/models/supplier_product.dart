import 'package:floor/floor.dart';

@entity
class SupplierProduct {
  SupplierProduct({
    required this.id,
    required this.name,
    required this.description,
    required this.type,
    required this.image,
    required this.expirationDate,
    required this.priceSupplier,
    required this.stock,
    required this.supplierId,
  });
  @PrimaryKey(autoGenerate: false)
  late final String id;
  late final String name;
  late final String description;
  late final String type;
  late final String image;
  late final String expirationDate;
  late final String priceSupplier;
  late final String stock;
  late final String supplierId;

  SupplierProduct.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['name'];
    description = json['description'];
    type = json['type'];
    image = json['image'];
    expirationDate = json['expirationDate'];
    priceSupplier = json['priceSupplier'].toString();
    stock = json['stock'].toString();
    supplierId = json['supplierId'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['_id'] = id;
    _data['name'] = name;
    _data['description'] = description;
    _data['type'] = type;
    _data['image'] = image;
    _data['expirationDate'] = expirationDate;
    _data['priceSupplier'] = priceSupplier.toString();
    _data['stock'] = stock.toString();
    _data['supplierId'] = supplierId;
    return _data;
  }

  SupplierProduct.fromMap(Map<String, dynamic> res)
      : id = res['_id'],
        name = res['name'],
        description = res['description'],
        type = res['type'],
        image = res['image'],
        expirationDate = res['expirationDate'],
        priceSupplier = res['priceSupplier'],
        stock = res['stock'],
        supplierId = res['supplierId'];

  Map<String, Object?> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'type': type,
      'image': image,
      'expirationDate': expirationDate,
      'priceSupplier': priceSupplier,
      'stock': stock,
      'supplierId': supplierId
    };
  }
}
