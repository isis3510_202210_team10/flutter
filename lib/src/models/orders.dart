import 'package:floor/floor.dart';

@entity
class Orders {
  Orders({
    required this.id,
    required this.initialDate,
    required this.arrivalDate,
    required this.address,
    required this.status,
    required this.total,
    required this.shopId,
    required this.supplierId,
  });
  @PrimaryKey(autoGenerate: false)
  late final String id;
  late final String initialDate;
  late final String arrivalDate;
  late final String address;
  late final String status;
  late final String total;
  late final String shopId;
  late final String supplierId;
  
  Orders.fromJson(Map<String, dynamic> json){
    id = json['_id'];
    initialDate = json['initialDate'];
    arrivalDate = json['arrivalDate'];
    address = json['address'];
    status = json['status'];
    total = json['total'].toString();
    shopId = json['shopId'];
    supplierId = json['supplierId'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['_id'] = id;
    _data['initialDate'] = initialDate;
    _data['arrivalDate'] = arrivalDate;
    _data['address'] = address;
    _data['status'] = status;
    _data['total'] = total;
    _data['shopId'] = shopId;
    _data['supplierId'] = supplierId;
    return _data;
  }

  Map<String, dynamic> toJsonCreate() {
    final _data = <String, dynamic>{};
    _data['initialDate'] = initialDate;
    _data['arrivalDate'] = arrivalDate;
    _data['address'] = address;
    _data['status'] = status;
    _data['total'] = total;
    _data['shopId'] = shopId;
    _data['supplierId'] = supplierId;
    return _data;
  }

  Orders.fromMap(Map<String, dynamic> res)
      : id = res['_id'],
        initialDate = res['initialDate'],
        arrivalDate = res['arrivalDate'],
        address = res['address'],
        status = res['status'],
        total = res['total'],
        shopId = res['shopId'],
        supplierId = res['supplierId'];

  Map<String, Object?> toMap() {
    return {
      'id': id,
      'initialDate': initialDate,
      'arrivalDate': arrivalDate,
      'address': address,
      'status': status,
      'total': total,
      'shopId': shopId,
      'supplierId': supplierId
    };
  }

}