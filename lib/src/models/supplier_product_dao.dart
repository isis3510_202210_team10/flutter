import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/supplier_product.dart';

@dao
abstract class SupplierProductDAO {
  @insert
  Future<List<int>> inserProduct(List<SupplierProduct> product);

  @Query('SELECT * FROM SupplierProduct')
  Future<List<SupplierProduct>> retrieveProducts();

  @Query('DELETE FROM SupplierProduct WHERE id = :id')
  Future<SupplierProduct?> deleteProduct(int id);

  @Query('DELETE FROM SupplierProduct')
  Future<List<SupplierProduct>> deleteProducts();
}
