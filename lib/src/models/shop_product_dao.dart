import 'package:floor/floor.dart';
import 'package:in_shop_flutter/src/models/shop_product.dart';

@dao
abstract class ShopProductDAO {
  @insert
  Future<List<int>> inserProduct(List<ShopProduct> product);

  @Query('SELECT * FROM ShopProduct')
  Future<List<ShopProduct>> retrieveProducts();

  @Query('DELETE FROM ShopProduct WHERE id = :id')
  Future<ShopProduct?> deleteProduct(int id);

  @Query('DELETE FROM ShopProduct')
  Future<List<ShopProduct>> deleteProducts();
}
