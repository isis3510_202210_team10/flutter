import 'package:floor/floor.dart';

@entity
class Supplier {
  Supplier({
    required this.id,
    required this.name,
    required this.email,
    required this.address,
    required this.phone,
    required this.companyName,
  });
  @PrimaryKey(autoGenerate: false)
  late final String id;
  late final String name;
  late final String email;
  late final String address;
  late final String phone;
  late final String companyName;

  Supplier.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    address = json['address'];
    phone = json['phone'];
    companyName = json['companyName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['_id'] = id;
    _data['name'] = name;
    _data['email'] = email;
    _data['address'] = address;
    _data['phone'] = phone;
    _data['companyName'] = companyName;
    return _data;
  }

  Supplier.fromMap(Map<String, dynamic> res)
      : id = res['_id'],
        name = res['name'],
        email = res['email'],
        address = res['address'],
        phone = res['phone'],
        companyName = res['companyName'];

  Map<String, Object?> toMap() {
    return {
      'id': id,
      'name': name,
      'email': email,
      'address': address,
      'phone': phone,
      'companyName': companyName
    };
  }
}
