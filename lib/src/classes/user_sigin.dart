/*
 * User class with type and id 
 */
class UserSignIn {
  // Attributes
  String type;
  String id;

  // Constructor
  UserSignIn(this.type, this.id);
}
