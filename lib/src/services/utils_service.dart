import 'package:in_shop_flutter/src/models/shop_product.dart';

class UtilsService {
  UtilsService._privateConstructor();

  static final UtilsService instance = UtilsService._privateConstructor();

  List<ShopProduct> obtenerProductosPorExpirar(List<ShopProduct> prods) {
    DateTime now = DateTime.now();
    DateTime dateMin = DateTime(now.year, now.month, now.day);
    DateTime dateMax = DateTime(now.year, now.month, now.day + 5);

    List<ShopProduct> res = [];

    for (int i = 0; i < prods.length; i++) {
      var date = DateTime.parse(prods[i].expirationDate);
      if (date.compareTo(dateMin) >= 0 && date.compareTo(dateMax) <= 0) {
        res.add(prods[i]);
      }
    }
    return res;
  }

  String respuestaExpiracion(List<ShopProduct> prods) {
    String rta = "";
    for (int i = 0; i < prods.length; i++) {
      rta += "-" +
          prods[i].name +
          " con id: " +
          prods[i].id.substring(prods[i].id.length - 4) +
          " vence el " +
          prods[i].expirationDate +
          "\n";
    }
    if (rta == "") {
      return "There are no products that expire in the next 5 days";
    }
    return rta;
  }
}
