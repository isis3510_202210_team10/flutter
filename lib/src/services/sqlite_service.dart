import 'package:in_shop_flutter/src/database/suppiler_product_database.dart';
import 'package:in_shop_flutter/src/database/supplier_database.dart';

import '../database/shop_product_database.dart';
import '../models/shop_product.dart';
import '../models/supplier_product.dart';
import '../models/orders.dart';
import '../models/supplier.dart';
import '../database/orders_database.dart';
import '../database/supplier_database.dart';

class SqliteService {
  SqliteService._privateConstructor();

  static final SqliteService instance = SqliteService._privateConstructor();

  // shop products
  localDatabaseAddShopProducts(
      ShopProductDatabase database, List<ShopProduct> products) async {
    $FloorShopProductDatabase
        .databaseBuilder('shop_product_database.db')
        .build()
        .then((value) async {
      database = value;
      await addShopProducts(database, products);
    });
  }

  Future<List<int>> addShopProducts(
      ShopProductDatabase db, List<ShopProduct> prods) async {
    return await db.shopProductDAO.inserProduct([...prods]);
  }

  Future<List<ShopProduct>> retrieveShopProducts(
      ShopProductDatabase database) async {
    return await database.shopProductDAO.retrieveProducts();
  }

  Future<List<ShopProduct>> deleteShopProducts(
      ShopProductDatabase database) async {
    return await database.shopProductDAO.deleteProducts();
  }

  // supplier products
  localDatabaseAddSupplierProducts(
      SupplierProductDatabase database, List<SupplierProduct> products) async {
    $FloorSupplierProductDatabase
        .databaseBuilder('supplier_product_database.db')
        .build()
        .then((value) async {
      database = value;
      await addSupplierProducts(database, products);
    });
  }

  Future<List<int>> addSupplierProducts(
      SupplierProductDatabase db, List<SupplierProduct> prods) async {
    return await db.supplierProductDAO.inserProduct([...prods]);
  }

  Future<List<SupplierProduct>> retrieveSupplierProducts(
      SupplierProductDatabase database) async {
    return await database.supplierProductDAO.retrieveProducts();
  }

  Future<List<SupplierProduct>> deleteSupplierProducts(
      SupplierProductDatabase database) async {
    return await database.supplierProductDAO.deleteProducts();
  }

  // order products
  localDatabaseAddOrders(OrdersDatabase database, List<Orders> orders) async {
    $FloorOrdersDatabase
        .databaseBuilder('orders_database.db')
        .build()
        .then((value) async {
      database = value;
      await addOrder(database, orders);
    });
  }

  Future<List<int>> addOrder(OrdersDatabase db, List<Orders> orders) async {
    return await db.ordersDAO.inserOrder([...orders]);
  }

  Future<List<Orders>> retrieveOrders(OrdersDatabase database) async {
    return await database.ordersDAO.retrieveOrders();
  }

  Future<List<Orders>> deleteOrders(OrdersDatabase database) async {
    return await database.ordersDAO.deleteOrders();
  }

  Future<List<int>> addSuppliers(
      SupplierDatabase db, List<Supplier> suppliers) async {
    return await db.supplierDAO.insertSupplier([...suppliers]);
  }

  Future<List<Supplier>> retrieveSuppliers(SupplierDatabase database) async {
    return await database.supplierDAO.retrieveSuppliers();
  }

  Future<List<Supplier>> deleteSuppliers(SupplierDatabase database) async {
    return await database.supplierDAO.deleteSuppliers();
  }
}
