import 'package:get_it/get_it.dart';
import 'package:in_shop_flutter/src/services/connectivity_service.dart';
import 'package:in_shop_flutter/src/services/isolate_service.dart';
import 'package:in_shop_flutter/src/services/shared_prefs_service.dart';
import 'package:in_shop_flutter/src/services/firebase_service.dart';
import 'package:in_shop_flutter/src/services/sqlite_service.dart';
import 'package:in_shop_flutter/src/services/utils_service.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerLazySingleton<MySharedPreferences>(
      () => MySharedPreferences.instance);
  getIt.registerLazySingleton<FirebaseService>(() => FirebaseService());
  getIt.registerLazySingleton<IsolateService>(() => IsolateService.instance);
  getIt.registerLazySingleton<UtilsService>(() => UtilsService.instance);
  getIt.registerLazySingleton<SqliteService>(() => SqliteService.instance);
  getIt.registerSingleton(ConnectivityService());
}
