import "package:http/http.dart" as http;
import 'dart:isolate';
import 'dart:convert';
import 'package:in_shop_flutter/src/models/shop_product.dart';
import 'package:in_shop_flutter/src/models/orders.dart';
import 'package:in_shop_flutter/src/models/supplier_product.dart';

class IsolateService {
  IsolateService._privateConstructor();

  static final IsolateService instance = IsolateService._privateConstructor();

  void computeTotalStockProducts(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    String? id = arguments[1].toString();
    var url =
        "https://inshop-uniandes.herokuapp.com/shops/" + id + "/shopProducts";
    var response = await http.get(Uri.parse(url));
    int totalStock = 0;
    int totalFood = 0;
    int totalCleaning = 0;
    int totalTechnology = 0;
    int totalClothing = 0;
    int totalStationary = 0;
    if (response.statusCode == 200) {
      try {
        var items = json.decode(response.body);
        List<ShopProduct> productos = List<ShopProduct>.from(
            items.map((producto) => ShopProduct.fromJson(producto)));
        for (var i = 0; i < productos.length; i++) {
          totalStock = totalStock + 1;
        }
        for (var i = 0; i < productos.length; i++) {
          if (productos[i].type == "FOOD") {
            totalFood = totalFood + 1;
          } else if (productos[i].type == "CLEANING") {
            totalCleaning = totalCleaning + 1;
          } else if (productos[i].type == "TECHNOLOGY" ||
              productos[i].type == "HARDWARE") {
            totalTechnology = totalTechnology + 1;
          } else if (productos[i].type == "CLOTING") {
            totalClothing = totalClothing + 1;
          } else if (productos[i].type == "STATIONERY") {
            totalStationary = totalStationary + 1;
          }
        }
      } catch (e) {
        totalStock.toString();
        totalStock = "Error calculating total stock" as int;
      }
    } else {
      totalStock.toString();
      totalStock = "Error calculating total stock price" as int;
    }
    List<int> totals = [
      totalStock,
      totalFood,
      totalCleaning,
      totalTechnology,
      totalClothing,
      totalStationary
    ];
    sendPort!.send(totals);
  }

  computeTotalPriceProducts(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    String? id = arguments[1].toString();
    var url =
        "https://inshop-uniandes.herokuapp.com/shops/" + id + "/shopProducts";
    var response = await http.get(Uri.parse(url));
    int totalPrice = 0;
    int totalPriceFood = 0;
    int totalPriceCleaning = 0;
    int totalPriceTechnology = 0;
    int totalPriceClothing = 0;
    int totalPriceStationary = 0;
    if (response.statusCode == 200) {
      try {
        var items = json.decode(response.body);
        List<ShopProduct> productos = List<ShopProduct>.from(
            items.map((producto) => ShopProduct.fromJson(producto)));
        for (var i = 0; i < productos.length; i++) {
          totalPrice = totalPrice + int.parse(productos[i].priceSupplier);
        }
        for (var i = 0; i < productos.length; i++) {
          if (productos[i].type == "FOOD") {
            totalPriceFood =
                totalPriceFood + int.parse(productos[i].priceSupplier);
          } else if (productos[i].type == "CLEANING") {
            totalPriceCleaning =
                totalPriceCleaning + int.parse(productos[i].priceSupplier);
          } else if (productos[i].type == "TECHNOLOGY" ||
              productos[i].type == "HARDWARE") {
            totalPriceTechnology =
                totalPriceTechnology + int.parse(productos[i].priceSupplier);
          } else if (productos[i].type == "CLOTING") {
            totalPriceClothing =
                totalPriceClothing + int.parse(productos[i].priceSupplier);
          } else if (productos[i].type == "STATIONERY") {
            totalPriceStationary =
                totalPriceStationary + int.parse(productos[i].priceSupplier);
          }
        }
      } catch (e) {
        totalPrice.toString();
        totalPrice = "Error calculating total inventory price" as int;
      }
    } else {
      totalPrice.toString();
      totalPrice = "Error calculating total inventory price" as int;
    }
    List<int> totals = [
      totalPrice,
      totalPriceFood,
      totalPriceCleaning,
      totalPriceTechnology,
      totalPriceClothing,
      totalPriceStationary
    ];
    sendPort!.send(totals);
  }

  fetchShopProducts(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    String? id = arguments[1].toString();
    var url =
        "https://inshop-uniandes.herokuapp.com/shops/" + id + "/shopProducts";
    print(url);
    var response = await http.get(Uri.parse(url));
    List<ShopProduct> productos = [];
    if (response.statusCode == 200) {
      try {
        var items = json.decode(response.body);
        productos = List<ShopProduct>.from(
            items.map((producto) => ShopProduct.fromJson(producto)));
        sendPort!.send(productos);
      } catch (e) {
        print(e);
      }
    }
  }

  fetchSupplierProducts(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    String? id = arguments[1].toString();
    var url = "https://inshop-uniandes.herokuapp.com/suppliers/" +
        id +
        "/productSuppliers";
    var response = await http.get(Uri.parse(url));
    List<SupplierProduct> productos = [];
    if (response.statusCode == 200) {
      try {
        var items = json.decode(response.body);
        productos = List<SupplierProduct>.from(
            items.map((producto) => SupplierProduct.fromJson(producto)));
        sendPort!.send(productos);
      } catch (e) {
        // ignore: avoid_print
        print(e);
      }
    }
  }

  fetchShopOrders(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    String? id = arguments[1].toString();
    var url = "https://inshop-uniandes.herokuapp.com/shops/" + id + "/orders";
    var response = await http.get(Uri.parse(url));
    List<Orders> orders = [];
    if (response.statusCode == 200) {
      try {
        var items = json.decode(response.body);
        orders =
            List<Orders>.from(items.map((order) => Orders.fromJson(order)));
        sendPort!.send(orders);
      } catch (e) {
        print(e);
      }
    }
  }

  fetchSupplierOrders(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    String? id = arguments[1].toString();
    var url =
        "https://inshop-uniandes.herokuapp.com/suppliers/" + id + "/orders";
    var response = await http.get(Uri.parse(url));
    List<Orders> orders = [];
    if (response.statusCode == 200) {
      try {
        var items = json.decode(response.body);
        orders =
            List<Orders>.from(items.map((order) => Orders.fromJson(order)));
        sendPort!.send(orders);
      } catch (e) {
        print(e);
      }
    }
  }

  updateProduct(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    ShopProduct? product = arguments[1] as ShopProduct?;
    print("Producto " + jsonEncode(product!.toJson()));
    var url =
        "https://inshop-uniandes.herokuapp.com/shopProducts/" + product.id;
    print(url);
    var response = await http.put(Uri.parse(url), body: product.toJsonCreate());
    if (response.statusCode == 200) {
      try {
        print("FINO");
        sendPort!.send(response.statusCode);
      } catch (e) {
        print(e);
      }
    } else {
      print("MALO");
      print(response.body);
    }
  }

  updateOrder(List<Object?> arguments) async {
    SendPort? sendPort = arguments[0] as SendPort?;
    Orders? order = arguments[1] as Orders?;
    print("Order " + jsonEncode(order!.toJson()));
    var url =
        "https://inshop-uniandes.herokuapp.com/orders/" + order.id;
    print(url);
    var response = await http.put(Uri.parse(url), body: order.toJsonCreate());
    if (response.statusCode == 200) {
      try {
        sendPort!.send(response.statusCode);
      } catch (e) {
        print(e);
      }
    } else {
      print(response.body);
    }
  }
}
