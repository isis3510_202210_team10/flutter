import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class FirebaseService {
  // Instance of firebase for authentication
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  // ignore: unused_field
  final FirebaseDatabase _database = FirebaseDatabase.instance;

  /*
   * Function in charge of carrying out the sign in of a user
   */
  Future<User?> signIn(String email, String password) async {
    User? user;
    try {
      UserCredential userCredential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      user = userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        // ignore: avoid_print
        print('No user found for that email');
      }
    }
    return user;
  }

  /*
   * Function in charge of carrying out the registration of a user
   */
  Future<String> signUp(String email, String password) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
      return 'Signed up';
    } catch (e) {
      return e.toString();
    }
  }
}
